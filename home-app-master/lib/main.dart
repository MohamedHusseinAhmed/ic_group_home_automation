import 'dart:io';

import 'package:flutter/material.dart';
import 'package:home_app/screens/home_page.dart';
import 'package:home_app/screens/ip_config.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/sensor.dart';
import 'package:home_app/services/mqtt/MQTTAppState.dart';
import 'package:home_app/services/mqtt/MQTTManager.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/acs_provider.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/curtain_provider.dart';
import 'package:home_app/services/provider/devices_provider.dart';
import 'package:home_app/services/provider/sensors_provider.dart';
import 'package:home_app/services/provider/user_provider.dart';
import 'package:home_app/theme/theme.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/routes.dart';
import 'package:provider/provider.dart';

import 'package:home_app/screens/splash.dart';
import 'package:shared_preferences/shared_preferences.dart';



void main() {


  runApp(MyApp());
}
String ip_conf;

class Reload extends StatefulWidget {
  static const String route = '/myapp';
  // landing pag fetch data and intilixw

  @override
  _reloadAppState createState() => _reloadAppState();
}


class _reloadAppState extends State<Reload> {



  @override
  Widget build(BuildContext context) {
    return MultiProvider(

      providers: [
        ChangeNotifierProvider<UserProvider>( builder: (_) =>  UserProvider()),
        ChangeNotifierProvider<CollectionProvider>(builder:  (_) => CollectionProvider()),
        ChangeNotifierProvider<DeviceProvider>(builder: (_) => DeviceProvider()),
        ChangeNotifierProvider<ACProvider>(builder: (_) => ACProvider()),
        ChangeNotifierProvider<CurtainProvider>(builder: (_) => CurtainProvider()),
        ChangeNotifierProvider<SensorProvider>(builder: (_) => SensorProvider()),
        ChangeNotifierProvider<ThemeChanger>(builder: (_) => ThemeChanger()),

      ],
      child: Consumer<ThemeChanger>(builder: (context, theme, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Easy Life',
          theme: theme.darkTheme ? darkTheme : lightTheme,
          routes: routes,
          //  home: HomeApp(),
        );
      }),
    );
  }

}
class MyApp extends StatelessWidget {
      // This widget is the root of application.

  @override
  Widget build(BuildContext context) {
    return MultiProvider(

      providers: [
        ChangeNotifierProvider<UserProvider>( builder: (_) =>  UserProvider()),
        ChangeNotifierProvider<CollectionProvider>(builder:  (_) => CollectionProvider()),
        ChangeNotifierProvider<DeviceProvider>(builder: (_) => DeviceProvider()),
        ChangeNotifierProvider<ACProvider>(builder: (_) => ACProvider()),
        ChangeNotifierProvider<CurtainProvider>(builder: (_) => CurtainProvider()),
        ChangeNotifierProvider<ThemeChanger>(builder: (_) => ThemeChanger()),
        ChangeNotifierProvider<SensorProvider>(builder: (_) => SensorProvider()),
      ],
      child: Consumer<ThemeChanger>(builder: (context, theme, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Easy Life',
          theme: theme.darkTheme ? darkTheme : lightTheme,
          routes: routes,
          //  home: HomeApp(),
        );
      }),
    );
  }
}


class HomeApp extends StatefulWidget {
   static const String route = '/';
  // landing pag fetch data and intilixw
  @override
  _HomeAppState createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  MQTTManager mqttServices;
  MQTTAppState currentAppState;
  CollectionServices _collectionServices=CollectionServices();
  SensorServices _sensorsServices=SensorServices();
  initData(data)async{

    await Provider.of<UserProvider>(context,listen:false).setUser(data);
  await Provider.of<CollectionProvider>(context,listen: false).setCollections(_collectionServices.getCollections(context));
    await Provider.of<SensorProvider>(context,listen: false).setSensors(_sensorsServices.getSensors(context));
  }

    load()async{
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    final String ip = myPrefs.getString('ip');
      ip_conf=ip;

    }
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<UserProvider>(context).currentUser(),
      builder:(context, snapshot) {
     load();
    if( ip_conf==""||ip_conf==null){
    return IPCONFIG();
    }else {
      Api k = new Api();

      if (snapshot.connectionState == ConnectionState.done) {
        if (snapshot.hasData) {
          //todo:fix bug when rerun "This ListenableProvider<UserProvider> widget cannot be marked as needing"
          initData(snapshot.data);
          return HomePage();
        } else {
          return LoginPage();
        }
      } else {
        return SplashPage();
      }
    }
      }, 
    );
  }
}






