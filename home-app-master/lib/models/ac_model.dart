import 'dart:convert';

AC acFromJson(String str) => AC.fromMap(json.decode(str));

String acToJson(AC data) => json.encode(data.toMap());

class AC {
    String id;
    DateTime createdAt;
    DateTime updatedAt;
    String macAddress;
    String tag;
    int version;
    int intensity;
    int group;
    int zone;
    int button;
    String value        ;
    String mode         ;
    int currentTemp  ;
    int targetTemp   ;
    int fanSpeed   ;
    int v;
    String name;
    String email;
    String picture;
    String createdBy;

    AC({
        this.id,
        this.createdAt,
        this.updatedAt,
        this.macAddress,
        this.tag,
        this.version,
        this.intensity,
        this.group,
        this.zone,
        this.button,
        this.value,
        this.mode,
        this.currentTemp,
        this.targetTemp,
        this.fanSpeed,
        this.v,
        this.name,
        this.email,
        this.picture,
        this.createdBy,
    });

    AC copyWith({
        String id,
        DateTime createdAt,
        DateTime updatedAt,
        String macAddress,
        String tag,
        int version,
        int intensity,
        int button,
        int group,
        int zone,
        String value,
        String mode,
        double currentTemp,
        double targetTemp,
        double fanSpeed,
        int v,
        String name,
        String email,
        String picture,
        String createdBy,
    }) => 
        AC(
            id: id ?? this.id,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            macAddress: macAddress ?? this.macAddress,
            tag: tag ?? this.tag,
            version: version ?? this.version,
            intensity: intensity ?? this.intensity,
            group: group ?? this.version,
            zone: zone ?? this.intensity,
            button: button ?? this.button,
            value: value ?? this.value,
            mode: mode ?? this.mode,
            currentTemp: currentTemp ?? this.currentTemp,
            targetTemp: targetTemp ?? this.targetTemp,
            fanSpeed: fanSpeed ?? this.fanSpeed,

            v: v ?? this.v,
            name: name ?? this.name,
            email: email ?? this.email,
            picture: picture ?? this.picture,
            createdBy: createdBy ?? this.createdBy,
        );

    factory AC.fromMap(Map<String, dynamic> json) => AC(
        id: json["_id"] == null ? null : json["_id"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        macAddress: json["macAddress"] == null ? null : json["macAddress"],
        tag: json["tag"] == null ? null : json["tag"],
        version: json["version"] == null ? null : json["version"],
        intensity: json["intensity"] == null ? null : json["intensity"],
        group: json["group"] == null ? null : json["group"],
        zone: json["zone"] == null ? null : json["zone"],
        button: json["button"] == null ? null : json["button"],
        value: json["value"] == null ? null : json["value"],
        mode: json["mode"] == null ? null : json["mode"],
        currentTemp: json["currentTemp"] == null ? null : int.parse(json["currentTemp"].toString()),
        targetTemp: json["targetTemp"] == null ? null : int.parse(json["targetTemp"].toString()),
        fanSpeed: json["fanSpeed"] == null ? null : int.parse(json["fanSpeed"].toString()),

        v: json["__v"] == null ? null : json["__v"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        picture: json["picture"] == null ? null : json["picture"],
        createdBy: json["createdBy"] == null ? null : json["createdBy"],
    );

    Map<String, dynamic> toMap() => {
        "_id": id == null ? null : id,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "macAddress": macAddress == null ? null : macAddress,
        "tag": tag == null ? null : tag,
        "version": version == null ? null : version,
        "intensity": intensity == null ? null : intensity,
        "group": group == null ? null : group,
        "zone": zone == null ? null : zone,
        "button": button == null ? null : button,
        "value": value == null ? null : value,
        "mode": mode == null ? null : mode,
        "currentTemp": currentTemp == null ? null : currentTemp,
        "targetTemp": targetTemp == null ? null : targetTemp,
        "fanSpeed": fanSpeed == null ? null : fanSpeed,
        "__v": v == null ? null : v,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "picture": picture == null ? null : picture,
        "createdBy": createdBy == null ? null : createdBy,
    };
}
