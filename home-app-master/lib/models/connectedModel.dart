import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


import 'appliance.dart';

class  ConnectedModel {
  List<Appliance> _applianceList = [
    Appliance(
      title : 'A/C',
      subtitle : '24 C',
      leftIcon : Icons.ac_unit,
      topRightIcon : Icons.threesixty,
      bottomRightIcon: Icons.threesixty,
      isEnable : true),
  ];
}
class ApplianceModel extends ConnectedModel {
  List<Appliance> get allYatch {
    return List.from(_applianceList);
  }
}