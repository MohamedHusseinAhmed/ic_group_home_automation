import 'package:flutter/material.dart';

import 'package:flutter_ijk/flutter_ijk.dart';
import 'package:home_app/services/api/api.dart';

class VideoIntercomPage_1 extends StatefulWidget {

  VideoIntercomPage_1();

  @override
  State<StatefulWidget> createState() {
    return VideoIntercomPage_1State();
  }
}

class VideoIntercomPage_1State extends State<VideoIntercomPage_1> {
  IjkPlayerController _controller;

  @override
  void initState(){
    super.initState();

    var  camera_rul =Api.Url.split('.');
        if(camera_rul.length==0)
          return;
     _controller = IjkPlayerController.network("rtsp://admin:admin1234@192.168.1.110:554/cam/realmonitor?channel=1&subtype=1")
      //_controller = IjkPlayerController.asset("video/big_buck_bunny.mp4")
        ..initialize().then((_) {
          setState(() {});
          _controller.play();
        });
  }


  @override
  Widget build(BuildContext context) {
    return Material(
      child: _controller == null ? Container(): Center(
        child:
          _controller.value.initialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: IjkPlayer(_controller),
                )
              : Container(),
      ),
    );
  }
  
  void _stop() async{
    if (_controller != null) {
      await _controller.dispose();
      _controller = null;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _stop();
  }
}
