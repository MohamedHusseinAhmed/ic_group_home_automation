import 'package:flutter/material.dart';

import 'package:flutter_ijk/flutter_ijk.dart';
import 'package:home_app/services/api/api.dart';

class VideoPage_2 extends StatefulWidget {

  VideoPage_2();

  @override
  State<StatefulWidget> createState() {
    return VideoPage_2State();
  }
}

class VideoPage_2State extends State<VideoPage_2> {
  IjkPlayerController _controller;

  @override
  void initState(){
    super.initState();

    var  camera_rul =Api.Url.split('.');
        if(camera_rul.length==0)
          return;
     _controller = IjkPlayerController.network("rtsp://admin:admin1234@"+camera_rul[0]+"."+camera_rul[1]+"."+camera_rul[2]+".244:554/cam/realmonitor?channel=1&subtype=1")
      //_controller = IjkPlayerController.asset("video/big_buck_bunny.mp4")
        ..initialize().then((_) {
          setState(() {});
          _controller.play();
        });
  }


  @override
  Widget build(BuildContext context) {
    return Material(
      child: _controller == null ? Container(): Center(
        child:
          _controller.value.initialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: IjkPlayer(_controller),
                )
              : Container(),
      ),
    );
  }
  
  void _stop() async{
    if (_controller != null) {
      await _controller.dispose();
      _controller = null;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _stop();
  }
}
