import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/pref_services.dart';
import 'package:http/http.dart' as http;

import 'auth.dart';

class DeviceServices extends PrefServices{
///index devices for current user
  ///
  AuthServices authServices = AuthServices();
  Future<List<Device>> getDevices(context) async {
    print('inside collections');
    String token = await getToken();
    print(token);
    String url = Api.devicesUrl;
    http.Response response =
        await http.get(Uri.parse(url), headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<Device> devices = List<Device>();
      data.forEach((element) {
        devices.add(Device.fromMap(element));
      });
      print(devices);
      return devices;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return null;
    }
  }
  Future<List<Device>> getdev(String id,context) async {
    List<dynamic> data = jsonDecode(id);
    List<Device> devices = List<Device>();
    data.forEach((element) {
      devices.add(Device.fromMap(element));
    });
    return devices;
  }

  ///get devices by ids
   Future<List<Device>> getDevicesByIds(List<String> ids,context) async {
    print(ids);
    String token = await getToken();
    Map<String, List<String>> body = {"devices": ids};
    String url = Api.devicesbyIdsUrl;
    http.Response response = await http.post(Uri.parse(url),
        headers: createAuthorizationHeader(token), body: json.encode(body));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<Device> devices = List<Device>();
      data.forEach((element) {
        devices.add(Device.fromMap(element));
      });
      return devices;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///Update Specific Device
  Future<bool> updateDevice(String deviceId,Device device,context) async {
    String url = Api.devicesUrl;
    String token = await getToken();

    http.Response response = await http.put(Uri.parse(url + deviceId),
        headers: createAuthorizationHeader(token),
        body: deviceToJson(device));
    if (response.statusCode == 200) {
        print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return false;
    }
  }

  ///delete device 
  Future deleteDevice(String deviceId,Device device,context) async{
    String url = Api.devicesUrl;
    device.id=deviceId;
    String token = await getToken();

    http.Response response = await http.delete(Uri.parse(url + deviceId),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return false;
    }
}



}


