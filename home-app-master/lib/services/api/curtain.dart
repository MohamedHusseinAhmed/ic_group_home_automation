import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:home_app/models/curtain_model.dart';
import 'package:home_app/screens/login_screen.dart';

import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/pref_services.dart';
import 'package:http/http.dart' as http;

import 'auth.dart';

class CurtainServices extends PrefServices{
  ///index curtains for current user
  AuthServices authServices = AuthServices();

  Future<List<Curtain>> getCurtains(context) async {
    print('inside collections');
    String token = await getToken();
    print(token);
    String url = Api.curtainsUrl;
    http.Response response =
    await http.get(Uri.parse(url), headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {

      List<dynamic> data = jsonDecode(response.body);
      List<Curtain> curtains = List<Curtain>();

      data.forEach((element) {
        curtains.add(Curtain.fromMap(element));
      });
      print(curtains);
      return curtains;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///get curtains by ids
  Future<List<Curtain>> getCurtainsByIds(List<String> ids,context) async {
    print(ids);
    String token = await getToken();
    Map<String, List<String>> body = {"curtains": ids};
    String url = Api.curtainsbyIdsUrl;
    http.Response response = await http.post(Uri.parse(url),
        headers: createAuthorizationHeader(token), body: json.encode(body));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<Curtain> curtains = List<Curtain>();
      data.forEach((element) {
        curtains.add(Curtain.fromMap(element));
      });
      return curtains;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///Update Specific Curtain
  Future<bool> updateCurtain(String curtainId,Curtain curtain,context) async {
    String url = Api.curtainsUrl;
    String token = await getToken();
    http.Response response = await http.put(Uri.parse(url + curtainId),
        headers: createAuthorizationHeader(token),
        body: curtainToJson(curtain));
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return false;
    }
  }

  ///delete Curtain

  Future deleteCurtain(String curtainId,Curtain curtain,context) async{
    String url = Api.curtainsUrl;
    curtain.id=curtainId;
    String token = await getToken();

    http.Response response = await http.delete(Uri.parse(url + curtainId),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return false;
    }
  }


}
