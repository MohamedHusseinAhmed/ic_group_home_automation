import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:home_app/models/collections.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/auth.dart';
import 'package:home_app/services/api/pref_services.dart';
import 'package:http/http.dart' as http;

class CollectionServices extends PrefServices {
  // AuthServices _authServices=AuthServices();
  //  CollectionServices._();
  AuthServices authServices = AuthServices();
  ///Get collections For current user
  Future<List<Collection>> getCollections(context) async {
    print('inside getcollections');
    String token = await getToken();
    String url = Api.collectionUrl;
    http.Response response =
        await http.get(Uri.parse(url), headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);

      print (data);

      List<Collection> collections = List<Collection>();

      print (collections);
      data.forEach((element) {
        collections.add(Collection.fromMap(element));
      });
      // print(collections);
      return collections;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }
    // else if(response.statusCode==401){
    //   //Todo:refresh session
    // }
     else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///create new collection in db
  Future<Collection> createCollection(Collection collection,context) async {
    print('inside createcollection');
    String token = await getToken();
    String url = Api.collectionUrl;
    http.Response response = await http.post(Uri.parse(url),
        headers: createAuthorizationHeader(token),
        body: collectionToJson(collection.copyWith(devices: [],curtains: [])));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);

      Collection collection = Collection.fromMap(data);


      return collection;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///update specific collection
  Future<Collection> updateCollection(String id, Map data,context) async {
    print('inside updatecollection');
    String token = await getToken();
    String url = Api.collectionUrl;
    print("collection id: $id ");
    http.Response response = await http.put(Uri.parse(url+ '$id') ,
        headers: createAuthorizationHeader(token), body: jsonEncode(data));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      Collection collection = Collection.fromMap(data);
      return collection;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///delete specific collection
  Future<bool> deleteCollection(String id,context) async {
    print('inside deletecollection');
    String token = await getToken();
    String url = Api.collectionUrl;
    http.Response response = await http.delete(
        Uri.parse(url + '$id'),
      headers: createAuthorizationHeader(token),
    );
    if (response.statusCode == 204) {
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///add device to specific collection
  Future<bool> addDeviceToCollection(String collectionId, String deviceId,context) async {
    print('inside addDeviceToCollection');
    String token = await getToken();
    String url = Api.deviceToCollectionUrl;
    print("collection id: $collectionId ");
    print("collection id: $deviceId ");
    var body = {
      "_id":deviceId
    };
    http.Response response = await http.put(Uri.parse(url + '$collectionId'),
        headers: createAuthorizationHeader(token), body: jsonEncode(body));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print('test addDeviceToCollection data ok : ' + data["nModified"].toString());
      return  data["nModified"]==1? true : false;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///remove device from specific collection
  Future<bool> removeDeviceFromCollection(String collectionId, String deviceId,context) async {
     print('inside remove device');
    String token = await getToken();
    String url = Api.collectionUrl;
    print("collection id: $collectionId ");
    http.Response response = await http.delete(Uri.parse(url + "/delete_device/"+'$collectionId/$deviceId'),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 204) {
      print('object true');
      return true ;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return false;
    }
  }






  ///add ac to specific collection
  Future<bool> addACToCollection(String collectionId, String acId,context) async {
    print('inside addACToCollection');
    String token = await getToken();
    String url = Api.acToCollectionUrl;
    print("collection id: $collectionId ");
    print("collection id: $acId ");
    var body = {
      "_id":acId
    };
    http.Response response = await http.put(Uri.parse(url + '$collectionId'),
        headers: createAuthorizationHeader(token), body: jsonEncode(body));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print('test addACToCollection data ok : ' + data["nModified"].toString());
      return  data["nModified"]==1? true : false;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///remove ac from specific collection
  Future<bool> removeACFromCollection(String collectionId, String acId,context) async {
    print('inside remove ac');
    String token = await getToken();
    String url = Api.collectionUrl;
    print("collection id: $collectionId ");
    http.Response response = await http.delete(Uri.parse(url + "delete_ac/"+'$collectionId/$acId'),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 204) {
      print('object true');
      return true ;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return false;
    }
  }





  ///add curtain to specific collection
  Future<bool> addCurtainToCollection(String collectionId, String curtainId,context) async {
    print('inside addCurtainToCollection');
    String token = await getToken();
    String url = Api.curtainToCollectionUrl;
    print("collection id: $collectionId ");
    print("collection id: $curtainId ");
    var body = {
      "_id":curtainId
    };
    http.Response response = await http.put(Uri.parse(url + '$collectionId'),
        headers: createAuthorizationHeader(token), body: jsonEncode(body));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print('test addCurtainToCollection data ok : ' + data["nModified"].toString());
      return  data["nModified"]==1? true : false;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///remove curtain from specific collection
  Future<bool> removeCurtainFromCollection(String collectionId, String curtainId,context) async {
    print('inside remove curtain');
    String token = await getToken();
    String url = Api.collectionUrl;
    print("collection id: $collectionId ");
    http.Response response = await http.delete(Uri.parse(url + "delete_curtain/"+'$collectionId/$curtainId'),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 204) {
      print('object true');
      return true ;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }  else {
      print('error${response.statusCode}');
      return false;
    }
  }
}
