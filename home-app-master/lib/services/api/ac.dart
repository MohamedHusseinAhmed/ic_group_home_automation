import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:home_app/models/ac_model.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/pref_services.dart';
import 'package:http/http.dart' as http;

import 'auth.dart';

class ACServices extends PrefServices{
///index acs for current user
  AuthServices authServices = AuthServices();
  Future<List<AC>> getACs(context) async {
    print('inside collections');
    String token = await getToken();
    print(token);
    String url = Api.acsUrl;
    http.Response response =
        await http.get(Uri.parse(url), headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<AC> acs = List<AC>();
      data.forEach((element) {
        acs.add(AC.fromMap(element));
      });
      print(acs);
      return acs;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }
    else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///get acs by ids
   Future<List<AC>> getACsByIds(List<String> ids,context) async {
    print(ids);
    String token = await getToken();
    Map<String, List<String>> body = {"acs": ids};
    String url = Api.acsbyIdsUrl;
    http.Response response = await http.post(Uri.parse(url),
        headers: createAuthorizationHeader(token), body: json.encode(body));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<AC> acs = List<AC>();
      data.forEach((element) {
        acs.add(AC.fromMap(element));
      });
      return acs;
    }else  if (response.statusCode == 401) {
    authServices.logOut().then((value) {
    Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(
    builder: (BuildContext context) => LoginPage()),
    (Route<dynamic> route) => false);
    }).catchError((onError) {
    print(onError);
    });
    }
    else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///Update Specific AC
  Future<bool> updateAC(String acId,AC ac,context) async {
    String url = Api.acsUrl;
    String token = await getToken();

    http.Response response = await http.put(Uri.parse(url + acId),
        headers: createAuthorizationHeader(token),
        body: acToJson(ac));
    if (response.statusCode == 200) {
        print(response.body);
      return true;
    }   else  if (response.statusCode == 401) {
    authServices.logOut().then((value) {
    Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(
    builder: (BuildContext context) => LoginPage()),
    (Route<dynamic> route) => false);
    }).catchError((onError) {
    print(onError);
    });
    }
    else {
      print('error${response.statusCode}');
      return false;
    }
  }

  ///delete ac
  Future deleteAC(String acId,AC ac,context) async{
    String url = Api.acsUrl;
    ac.id=acId;
    String token = await getToken();

    http.Response response = await http.delete(Uri.parse(url + acId),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
    authServices.logOut().then((value) {
    Navigator.of(context).pushAndRemoveUntil(
    MaterialPageRoute(
    builder: (BuildContext context) => LoginPage()),
    (Route<dynamic> route) => false);
    }).catchError((onError) {
    print(onError);
    });
    }
    else {
    print('error${response.statusCode}');
    return false;
    }
    }
}
