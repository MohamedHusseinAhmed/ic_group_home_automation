import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_app/models/sensor_model.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/pref_services.dart';
import 'package:http/http.dart' as http;

import 'auth.dart';

class SensorServices extends PrefServices{
  ///index sensors for current user
  ///
  ///
  AuthServices authServices = AuthServices();
  Future<List<Sensor>> getSensors(context) async {
    print('inside collections');
    String token = await getToken();
    print(token);
    String url = Api.sensorsUrl;
    http.Response response =
    await http.get(Uri.parse(url), headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<Sensor> sensors = List<Sensor>();
      data.forEach((element) {
        sensors.add(Sensor.fromMap(element));
      });
      print(sensors);
      return sensors;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    }else
   {
      print('error${response.statusCode}');
      return null;
    }
  }


  ///get sensors by ids
  Future<List<Sensor>> getSensorsByIds(List<String> ids,context) async {
    print(ids);
    String token = await getToken();
    Map<String, List<String>> body = {"sensors": ids};
    String url = Api.sensorsbyIdsUrl;
    http.Response response = await http.post(Uri.parse(url),
        headers: createAuthorizationHeader(token), body: json.encode(body));
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<Sensor> sensors = List<Sensor>();
      data.forEach((element) {
        sensors.add(Sensor.fromMap(element));
      });
      return sensors;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return null;
    }
  }

  ///Update Specific Sensor
  Future<bool> updateSensor(String sensorId,Sensor sensor,context) async {
    String url = Api.sensorsUrl;
    String token = await getToken();

    http.Response response = await http.put(Uri.parse(url + sensorId),
        headers: createAuthorizationHeader(token),
        body: sensorToJson(sensor));
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return false;
    }
  }

  ///delete sensor
  Future deleteSensor(String sensorId,Sensor sensor,context) async{
    String url = Api.sensorsUrl;
    sensor.id=sensorId;
    String token = await getToken();

    http.Response response = await http.delete(Uri.parse(url + sensorId),
        headers: createAuthorizationHeader(token));
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    }else  if (response.statusCode == 401) {
      authServices.logOut().then((value) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
      }).catchError((onError) {
        print(onError);
      });
    } else {
      print('error${response.statusCode}');
      return false;
    }
  }



}
