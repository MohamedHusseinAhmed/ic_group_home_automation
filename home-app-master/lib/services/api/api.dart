
import 'dart:io';
import 'dart:math';

import 'package:home_app/services/mqtt/MQTTAppState.dart';
import 'package:home_app/services/mqtt/MQTTManager.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// api Endpoints
class Api {


  //Api engine
  MQTTAppState currentAppState;
  static MQTTManager mqttServices;
  static   String Url;
  static   String baseUrl ;//= 'http://192.168.42.201:9000/';
  static   String uploadUrl = baseUrl+ 'upload/';
  static  String userUrl = baseUrl + 'api/v1/users/';
  static   String userUpdateUrl = baseUrl + 'api/v1/users/update/';
  static   String devicesUrl = baseUrl + 'api/v1/devices/';
  static   String devicesbyIdsUrl = baseUrl + 'api/v1/devices/ids/';
  static   String collectionUrl = baseUrl + 'api/v1/collections/';
  static   String deviceToCollectionUrl = baseUrl + 'api/v1/collections/add_device/';
  static   String authUrl = baseUrl + 'auth/local/';

  static   String curtainsUrl = baseUrl + 'api/v1/curtains/';
  static   String curtainsbyIdsUrl = baseUrl + 'api/v1/curtains/ids/';
  static   String curtainToCollectionUrl = baseUrl + 'api/v1/collections/add_curtain/';

  static   String acsUrl = baseUrl + 'api/v1/acs/';
  static   String acsbyIdsUrl = baseUrl + 'api/v1/acs/ids/';
  static   String acToCollectionUrl = baseUrl + 'api/v1/collections/add_ac/';

  static   String sensorsUrl = baseUrl + 'api/v1/sensors/';
  static   String sensorsbyIdsUrl = baseUrl + 'api/v1/sensors/ids/';
  static   String sensorToCollectionUrl = baseUrl + 'api/v1/collections/add_sensor/';


  Api() {

   load();
  }
  void _configureAndConnect() {
    // TODO: Use UUID
    String osPrefix = 'Flutter_iOS';
    if(Platform.isAndroid){
      osPrefix = 'Flutter_Android';
    }
    mqttServices = MQTTManager(
        host: Url,
        identifier: 'flutter${Random.secure().nextInt(1000)}',
        state: currentAppState);
    mqttServices.initializeMQTTClient();
    mqttServices.connect();
  }


  load()async{
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    final String ip = myPrefs.getString('ip');
    final String port = myPrefs.getString('port');
    baseUrl="http://"+ip+":"+port+"/";
    Url=ip;
    _configureAndConnect();
  }


}

  Map<String, String> createAuthorizationHeader(String token) {
    return {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
      'Accept-Language': 'en_US'
    };
  }