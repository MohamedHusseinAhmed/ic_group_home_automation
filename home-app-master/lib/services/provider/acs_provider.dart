import 'package:flutter/material.dart';
import 'package:home_app/models/ac_model.dart';


class ACProvider with ChangeNotifier {
  
  AC _ac = AC();
  Future<List<AC>> _acs;
  int _acNumber;

  int get acNumber => _acNumber;
  setACNumber(int acNum) {
    _acNumber = acNum;
    notifyListeners();
  }

  AC get ac => _ac;
  setAC(AC ac) {
    this._ac = ac;
    notifyListeners();
  }

  Future<List<AC>> get acs => _acs;
  setACs(Future<List<AC>> acs) {
    this._acs = acs;
    notifyListeners();
  }
}
