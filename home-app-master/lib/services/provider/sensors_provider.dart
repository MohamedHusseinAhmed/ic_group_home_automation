import 'package:flutter/material.dart';
import 'package:home_app/models/sensor_model.dart';

class SensorProvider with ChangeNotifier {

  Sensor _sensor = Sensor();
  Future<List<Sensor>> _sensors;
  int _sensorNumber;

  int get sensorNumber => _sensorNumber;
  setSensorNumber(int sensorNum) {
    _sensorNumber = sensorNum;
    notifyListeners();
  }

  Sensor get sensor => _sensor;
  setSensor(Sensor sensor) {
    this._sensor = sensor;
    notifyListeners();
  }

  Future<List<Sensor>> get sensors => _sensors;
  setSensors(Future<List<Sensor>> sensors) {
    this._sensors = sensors;



    notifyListeners();
  }
}
