import 'package:flutter/material.dart';
import 'package:home_app/models/curtain_model.dart';
class CurtainProvider with ChangeNotifier {

  Curtain _curtain = Curtain();
  Future<List<Curtain>> _curtains;
  int _curtainNumber;

  int get curtainNumber => _curtainNumber;
  setCurtainNumber(int curtainNum) {
    _curtainNumber = curtainNum;
    notifyListeners();
  }

  Curtain get curtain => _curtain;
  setCurtain(Curtain curtain) {
    this._curtain = curtain;
    notifyListeners();
  }

  Future<List<Curtain>> get curtains => _curtains;
  setCurtains(Future<List<Curtain>> curtains) {
    this._curtains = curtains;
    notifyListeners();
  }
}


