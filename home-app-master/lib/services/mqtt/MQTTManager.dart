import 'package:flutter/cupertino.dart';
import 'package:home_app/models/ac_model.dart';
import 'package:home_app/models/curtain_model.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/models/sensor_model.dart';
import 'package:home_app/screens/room_page.dart';
import 'package:home_app/services/mqtt/MQTTAppState.dart';
import 'package:mqtt_client/mqtt_client.dart';

import 'package:mqtt_client/mqtt_server_client.dart';

class MQTTManager{

  // Private instance of client
  final MQTTAppState _currentState;
    RoomPage room;
  MqttServerClient client;
  final String _identifier;
  final String _host;


  // Constructor
  MQTTManager({
   @required String host,
    @required String topic,
    @required String identifier,
    @required MQTTAppState state
}
): _identifier = identifier, _host = host, _currentState = state ;




  void initializeMQTTClient(){
    client = MqttServerClient(_host,_identifier);
    client.port = 1883;

    client.onDisconnected = onDisconnected;
    client.onSubscribed = onSubscribed;

    client.logging(on: true);

    /// Add the successful connection callback
    client.onConnected = onConnected;
    client.onSubscribed = onSubscribed;

    final MqttConnectMessage connMess = MqttConnectMessage()
        .withClientIdentifier(_identifier)
        .withWillTopic('willtopic') // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.atMostOnce);
    print('EXAMPLE::Mosquitto client connecting....');
    client.connectionMessage = connMess;


  }
  // Connect to the host
  void connect() async{
    assert(client != null);
    try {
      print('EXAMPLE::Mosquitto start client connecting....');
    //  _currentState.setAppConnectionState(MQTTAppConnectionState.connecting);
      await client.connect("username", "password");
    } on Exception catch (e) {
      print('EXAMPLE::client exception - $e');
      disconnect();
    }
  }

  void disconnect() {
    print('Disconnected');
    client.disconnect();
  }
  


  /// The subscribed callback
  void onSubscribed(String topic) {
    print('EXAMPLE::Subscription confirmed for topic $topic');
  }

  /// The unsolicited disconnect callback
  void onDisconnected() {
    print('EXAMPLE::OnDisconnected client callback - Client disconnection');
    if (client.connectionStatus.returnCode == MqttConnectReturnCode.noneSpecified) {
      print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
    }
   // _currentState.setAppConnectionState(MQTTAppConnectionState.disconnected);
  }

  /// The successful connect callback
  void onConnected() {
   // _currentState.setAppConnectionState(MQTTAppConnectionState.connected);
    print('EXAMPLE::Mosquitto client connected....');

    client.subscribe("devices/#", MqttQos.atMostOnce);
    client.subscribe("acs/#", MqttQos.atMostOnce);
    client.subscribe("curtains/#", MqttQos.atMostOnce);
    client.subscribe("sensors/#", MqttQos.atMostOnce);



    print(
        'EXAMPLE::OnConnected client callback - Client connection was sucessful');
  }



  Future<void> conChk()
  async {

    if (client.connectionStatus.state == MqttConnectionState.connected) {
      print('EXAMPLE::Mosquitto client connected');

    } else {
      /// Use status here rather than state if you also want the broker return code.
      print(
          'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');

      try {
        await client.connect("username", "password");


      } on Exception catch (e) {
        print('EXAMPLE::client exception - $e');
        client.disconnect();
      }



      //exit(-1);
    }
  }
  void toggleDevice(String deviceId, Device device) {

    conChk();

    print('device:$deviceId :${deviceToJson(device) }');
    final builder = MqttClientPayloadBuilder();
    conChk();
    builder.addString(deviceToJson(device));
    client.publishMessage(
        'devices/$deviceId', MqttQos.atMostOnce, builder.payload);
  }
  void toggleCurtain(String deviceId,Curtain curtain) {
    conChk();
    print('device:$deviceId :${curtainToJson(curtain) } ');
    final builder = MqttClientPayloadBuilder();

    builder.addString(curtainToJson(curtain));
    client.publishMessage(
        'curtains/$deviceId', MqttQos.atMostOnce, builder.payload);
  }
  void toggleAC(String deviceId,AC ac) {
    conChk();
    print('device:$deviceId :${acToJson(ac) }');
    final builder = MqttClientPayloadBuilder();

    builder.addString(acToJson(ac));
    client.publishMessage(
        'acs/$deviceId', MqttQos.atMostOnce, builder.payload);
  }

  void toggleSensor(String deviceId,Sensor sensor) {
    conChk();
    print('device:$deviceId :${sensorToJson(sensor) }');
    final builder = MqttClientPayloadBuilder();

    builder.addString(sensorToJson(sensor));
    client.publishMessage(
        'sensors/$deviceId', MqttQos.atMostOnce, builder.payload);
  }
}