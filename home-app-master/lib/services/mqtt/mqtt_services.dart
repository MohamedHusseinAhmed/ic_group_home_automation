
import 'dart:io';
import 'dart:convert';

// import 'package:mqtt_client/mqtt_browser_client.dart'; //comment for android
import 'package:home_app/models/ac_model.dart';
import 'package:home_app/models/curtain_model.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/services/api/api.dart';

import 'package:home_app/services/provider/devices_provider.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:provider/provider.dart';


import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter/foundation.dart' show kIsWeb; //comment for android

const bool kIsWeb = false ; //uncomment for android

class MqttServices {



  ///use in run for web

  static const String webHost = 'wss://192.168.1.6:1883';
  static  String host  ; // "192.168.1.2";
  static const String username = 'username';
  static const String password = 'password';
  static const String clientId = 'AndroidApp';
  static const String webClientId = 'WebApp';
  static const int webPort = 1883;
  static const int port = 1883;
  static const int keepalive = 60;
  final client ;

//uncomment for android
   MqttServices._() :
        client =  MqttServerClient.withPort("", clientId, port) ;

  ///uncomment for web
// MqttServices._()
//       : client = kIsWeb
//             ? MqttBrowserClient.withPort(webHost, webClientId, webPort)
//             : MqttServerClient.withPort(host, clientId, port);


   static final   MqttServices _singleton = MqttServices();


  factory MqttServices() {

   return _singleton;
  }

  Future<String> ipconf() async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    final String ip = myPrefs.getString('ip');
    return ip;
  }

  void initClient() async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
     String ip = myPrefs.getString('ip');
     // client = MqttServerClient.withPort(ip, 'AndroidApp', 1883);
     client.logging(on: true);
    client.keepAlivePeriod = keepalive;
    client.onDisconnected = onDisconnected;
    client.onConnected = onConnected;
    client.onSubscribed = onSubscribed;
    client.onSubscribeFail = onSubscribeFail;
    client.pongCallback = pong;

    final connMess = MqttConnectMessage()
        .withClientIdentifier(clientId)
        .keepAliveFor(30) // Must agree with the keep alive set above or not set
        .withWillTopic(
        'willtopic') // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.atMostOnce);
    print('EXAMPLE::Mosquitto client connecting....');
     client.connectionMessage = connMess;





    try {
      await client.connect(username, password);


    } on Exception catch (e) {
      print('EXAMPLE::client exception - $e');
      client.disconnect();
    }

    if (client.connectionStatus.state == MqttConnectionState.connected) {
      print('EXAMPLE::Mosquitto client connected');
    } else {
      /// Use status here rather than state if you also want the broker return code.
      print(
          'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');
      client.disconnect();
      //exit(-1);
    }
  }


  Future<void> conChk()
   async {

    if (client.connectionStatus.state == MqttConnectionState.connected) {
      print('EXAMPLE::Mosquitto client connected');

    } else {
      /// Use status here rather than state if you also want the broker return code.
      print(
          'EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');

      try {
     await client.connect(username, password);


      } on Exception catch (e) {
        print('EXAMPLE::client exception - $e');
        client.disconnect();
      }



      //exit(-1);
    }
  }
  void pong() {
    print('Ping response client callback invoked');
  }
  /// The subscribed callback
  void onSubscribed(String topic) {


    print('EXAMPLE::Subscription confirmed for topic $topic');
  }

  /// The unsolicited disconnect callback
  void onDisconnected() {
    print('EXAMPLE::OnDisconnected client callback - Client disconnection');
    if (client.connectionStatus.returnCode == MqttConnectReturnCode.solicited) {
      print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
    }
    conChk();

 //   exit(-1);
  }
  void onSubscribeFail(String topic) {
    print('Failed to subscribe $topic');
  }
  /// The successful connect callback
  void onConnected() {
    client.subscribe("devices/#", MqttQos.atMostOnce);
    client.subscribe("acs/#", MqttQos.atMostOnce);
    client.subscribe("curtains/#", MqttQos.atMostOnce);
    client.subscribe("sensors/#", MqttQos.atMostOnce);
    print(
        'EXAMPLE::OnConnected client callback - Client connection was sucessful');
  }

  void toggleDevice(String deviceId, Device device) {



    print('device:$deviceId :${deviceToJson(device) }');
    final builder = MqttClientPayloadBuilder();
    conChk();
    builder.addString(deviceToJson(device));
    client.publishMessage(
        'devices/$deviceId', MqttQos.atMostOnce, builder.payload);
  }
  void toggleCurtain(String deviceId,Curtain curtain) {
    conChk();
    print('device:$deviceId :${curtainToJson(curtain) } ');
    final builder = MqttClientPayloadBuilder();
    conChk();
    builder.addString(curtainToJson(curtain));
    client.publishMessage(
        'curtains/$deviceId', MqttQos.atMostOnce, builder.payload);
  }
  void toggleAC(String deviceId,AC ac) {
    conChk();
    print('device:$deviceId :${acToJson(ac) }');
    final builder = MqttClientPayloadBuilder();

    builder.addString(acToJson(ac));
    client.publishMessage(
        'acs/$deviceId', MqttQos.atMostOnce, builder.payload);
  }
}