import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:home_app/models/credentials.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/auth.dart';
import 'package:home_app/services/api/pref_services.dart';
import 'package:http/http.dart' as http;

class APModeServices extends PrefServices {
  static   String host = "http://192.168.4.1";
  static   String root = Api.sensorsUrl;
  AuthServices authServices = AuthServices();
// {"ssid":"","wpa2":"","email":"","password":""}
  Future<String> sendData(String name) async {
    // print('inside send data to esp8266');
    String token = await getToken();
    Credentials credentials = credentialsFromJson(await getCredentials());
    var body = {
      "email": credentials.email,
      "password": credentials.password,
      "name": name,
      "value": "on"
    };
    http.Response response = await http.post(Uri.parse(root),headers: createAuthorizationHeader(token), body: json.encode(body));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);

      return "200";
    }else  if (response.statusCode == 401) {
      var data = jsonDecode(response.body);


      print("Unauthrized");
      return "401";
    }  else {
      print('error${response.statusCode}');
      return null;
    }
  }
}
