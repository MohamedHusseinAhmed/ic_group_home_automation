class Assets{
  static const String menuIcon='assets/icons/menu.png';
  static const String nightImage='assets/images/night.jpg';
  static const String morningImage='assets/images/morning.jpg';
  static const String eveningImage='assets/images/evening.jpeg';
  static const String roomImage='assets/images/room.jpg';
  static const String temperatureIcon='assets/icons/temperature.png';
  static const String humidityIcon='assets/icons/humidity.png';
  static const String aboutIcon='assets/icons/about.png';
  static const String deviceIcon='assets/icons/device.png';
  static const String moonIcon='assets/icons/moon.png';
  static const String homeIcon='assets/icons/home.png';
  static const String logoutIcon='assets/icons/log-out.png';
  static const String up='assets/icons/up.png';
  static const String down='assets/icons/down.png';
  static const String fan='assets/icons/fan.png';
  static const String drop='assets/icons/drops.png';
  static const String stop='assets/icons/stop.png';

  static const String gas='assets/icons/gas.png';
  static const String fire='assets/icons/fire_sensor.png';
  static const String alert='assets/icons/alert.png';
  static const String motion='assets/icons/motion-sensor.png';
  static const String touch='assets/icons/touch.png';
  static const String safe='assets/icons/safe.png';
  static const String window='assets/icons/window.png';
  static const String lampon='assets/icons/lampOn.png';
  static const String lampoff='assets/icons/lampOFF.png';

  static const String logo='assets/images/bootlogo.PNG';
}