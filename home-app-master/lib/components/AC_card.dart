import 'package:flutter/material.dart';
import 'package:home_app/components/icon_button.dart';
import 'package:home_app/models/connectedModel.dart';
import 'package:home_app/utils/assets.dart';

class ACCard extends StatelessWidget {
  const ACCard({
    Key key,
    this.color,
    this.onPower,
    this.onChange,
    this.name,
    this.status, this.onLongPress, this.onMode, this.onFan, this.currentT, this.TargetT,   this.onadd, this.onremove,   this.fanup, this.fandown, this.ModeIcon, this.fanIcon,
  }) : super(key: key);
  final VoidCallback onPower;
  final VoidCallback onChange;
  final VoidCallback onLongPress;

  final VoidCallback onMode;
  final VoidCallback onFan;
  final VoidCallback onadd;
  final VoidCallback onremove;
  final Icon ModeIcon;
  final Icon fanIcon;
  final VoidCallback fanup;
  final VoidCallback fandown;
  final int currentT;
  final int TargetT;

  final Color color;



  final String name;
  final String status;

  //double currentSlider;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: color,
      height: 20,
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      onPressed:null,
      onLongPress: onLongPress,
      child: GridTile(

        header: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: [

            Row(   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [




                RectIconButton(
                    height: 40,
                    width: 40,
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                        child: Icon(
                          Icons.power_settings_new,
                          color: color,
                        )),
                    onPressed: onPower)
                  , RectIconButton(
                    height: 40,
                    width: 40,
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                        child: ModeIcon
                    ),
                    onPressed: null),
                Text(
                  '$name',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      .copyWith(color: Theme.of(context).accentColor),
                ),
                RectIconButton(
                    height: 40,
                    width: 40,
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                      child:  fanIcon
                      ,
                    ),
                    onPressed: null)


              ],



            ),



            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(),
                    child: Divider(
                      color: Colors.black,
                      height: 36,
                    )),
              ),


            ]),
            Row(   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  Text(
                    '$TargetT'+" °C",
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(fontSize: 70),

                  ),
                   ]),  Text(
              "Current Temp :"+'$currentT'+" °C",
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(fontSize: 20),
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(),
                    child: Divider(
                      color: Colors.black,
                      height: 36,
                    )),
              ),


            ]),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [

                RectIconButton(
                    height: 50,
                    width: 50,
                    color: Theme.of(context).backgroundColor,
                    child: Center(

                        child:Text(
                          "Mode" ,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1
                              .copyWith(fontSize: 15),

                        )


                    ),
                    onPressed: onMode),
                RectIconButton(
                    height: 50,
                    width: 50,
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                      child: Icon(
                        Icons.flip_camera_android,
                        color: color,
                      ),
                    ),
                    onPressed: onFan),
                RectIconButton(
                    height: 50,
                    width: 50,
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                      child:  Icon(
                        Icons.arrow_upward_outlined,
                        color: color,
                      ),
                    ),
                    onPressed: onadd),
                RectIconButton(

                    height: 50,
                    width: 50,
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                        child: Icon(
                          Icons.arrow_downward_sharp,
                          color: color,
                        )),
                    onPressed: onremove)

              ],), Text(
              "Power : "+'$status',
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(fontSize: 20),
            ),


          ],)


        ,

        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            borderRadius: BorderRadius.circular(18.0),
          ),

        ),

      ),
    );
  }
}

/*
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 20,
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      onPressed:null,
      onLongPress: onLongPress,
      child: GridTile(

          header: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            children: [

              Row(   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

              CircularIconButton(
                  height: 28,
                  width: 28,
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                    child:  Icon(
                    Icons.ac_unit_rounded,
                    color: Theme.of(context).accentColor,
                  )

                    ,
                  ),
                  onPressed: null),

          Text(
          '$name',
          style: Theme.of(context)
          .textTheme
          .subtitle2
          .copyWith(color: Theme.of(context).accentColor),
               ),
              CircularIconButton(
                  height: 45,
                  width: 45,
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                      child: Icon(
                    Icons.power_settings_new,
                    color: color,
                  )),
                  onPressed: onPower)





            ],



          ),
              Row(   mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CircularIconButton(
                        height: 40,
                        width: 40,
                        color: Theme.of(context).backgroundColor,
                        child: Center(
                            child: Icon(
                              Icons.remove,
                              color: Theme.of(context).accentColor,
                            )),
                        onPressed: fandown)

,
                    CircularIconButton(
                        height: 15,
                        width: 15,

                        color: Theme.of(context).backgroundColor,child: Center(child: Icon(Icons.circle,
                      color: l1,  )), onPressed: null),
                    CircularIconButton(
                        height: 25,
                        width: 25,
                        color: Theme.of(context).backgroundColor,child: Center(child: Icon(Icons.circle,
                      color: l2, )), onPressed: null),
                    CircularIconButton(
                        height: 35,
                        width: 35,
                        color:Theme.of(context).backgroundColor,child: Center(child: Icon(Icons.circle,
                      color: l3, )), onPressed: null),
                    CircularIconButton(
                        height: 45,
                        width: 45,
                        color: Theme.of(context).backgroundColor,child: Center(child: Icon(Icons.circle,
                      color: l4,  )), onPressed: null)


                    ,
                    CircularIconButton(
                        height: 40,
                        width: 40,
                        color: Theme.of(context).backgroundColor,
                        child: Center(
                            child: Icon(
                              Icons.add_rounded,
                              color: Theme.of(context).accentColor,
                            )),
                        onPressed: fanup), ]),




              Row(   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CircularIconButton(
                        height: 40,
                        width: 40,
                        color: Theme.of(context).backgroundColor,
                        child: Center(
                            child: Icon(
                              Icons.remove,
                              color: Theme.of(context).accentColor,
                            )),
                        onPressed: onremove)

                    ,

                    Text(
                      '$TargetT'+" °C",
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .copyWith(fontSize: 20),

                    ),
                    CircularIconButton(
                        height: 40,
                        width: 40,
                        color: Theme.of(context).backgroundColor,
                        child: Center(
                            child: Icon(
                              Icons.add_rounded,
                              color: Theme.of(context).accentColor,
                            )),
                        onPressed: onadd), ]),   Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CircularIconButton(
                      height: 40,
                      width: 40,
                      color: Theme.of(context).backgroundColor,
                      child: Center(
                          child: Icon(
                            Icons.ac_unit_rounded,
                            color: ColorCool,
                          )),
                      onPressed: onCool),
                  CircularIconButton(
                      height: 40,
                      width: 40,
                      color: Theme.of(context).backgroundColor,
                      child: Center(
                        child: Image.asset(
                          Assets.fan,
                          scale: 1.5,
                          color: ColorFan,
                        ),
                      ),
                      onPressed: onFan),
                  CircularIconButton(
                      height: 40,
                      width: 40,
                      color: Theme.of(context).backgroundColor,
                      child: Center(
                        child: Image.asset(
                          Assets.drop,
                          scale: 1.5,
                          color: ColorDry,
                        ),
                      ),
                      onPressed: onDry),
                  CircularIconButton(
                      height: 40,
                      width: 40,
                      color: Theme.of(context).backgroundColor,
                      child: Center(
                          child: Icon(
                            Icons.wb_sunny_outlined,
                            color: ColorHeat,
                          )),
                      onPressed: onHeat)

                ],), Text(
                "Power : "+'$status',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(fontSize: 20),
              ),
              Text(
                "Current Temp :"+'$currentT'+" °C",
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(fontSize: 20),
              ),

        ],)


         ,

          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(18.0),
            ),

          ),

      ),
    );
  }
}
*/