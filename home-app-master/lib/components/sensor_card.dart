import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_app/components/icon_button.dart';
import 'package:home_app/utils/assets.dart';

class SensorCard extends StatelessWidget {
  const SensorCard({
    Key key,
    this.color,
    this.onTap,
    this.name,
    this.status, this.onLongPress, this.asset, this.state, this.sswitch,
  }) : super(key: key);
  final VoidCallback onTap;
  final VoidCallback onLongPress;
  final Color color;
  final String name;
  final String status;
final String asset;
  final String  state;
  final Container sswitch;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
onPressed: onTap,
      child: GridTile(
          header: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CircularIconButton(
                  height: 28,
                  width: 28,
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                    child:  Image.asset(
                      asset,
                      scale: 2,

                    ),
                  ),
                  onPressed: null),

            ],
          ),
          footer: Padding(
            padding: const EdgeInsets.only(bottom: 20.0, left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Center(
                  child:  Image.asset(
                    state,
                    scale: 6.5,

                  ),
                ),




                Center(
                  child:  Text(
                    '$name',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(color: Theme.of(context).accentColor,fontSize: 15),
                  ),
                ),
                sswitch,

              ],
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(18.0),
            ),
          )),
    );
  }
}
