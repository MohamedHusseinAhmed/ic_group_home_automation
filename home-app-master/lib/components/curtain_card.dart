import 'package:flutter/material.dart';
import 'package:home_app/components/icon_button.dart';
import 'package:home_app/utils/assets.dart';

class CurtainCard extends StatelessWidget {
  const CurtainCard({
    Key key,
    this.colorUp,
    this.colorDown,
    this.colorStop,
    this.onTap,
    this.name,
    this.status, this.onLongPress,
    this.onUp,
    this.onDwon,
    this.onStop,
  }) : super(key: key);
  final VoidCallback onTap;
  final VoidCallback onUp;
  final VoidCallback onDwon;
  final VoidCallback onStop;
  final VoidCallback onLongPress;
  final Color colorUp;
  final Color colorDown;
  final Color colorStop;
  final String name;
  final String status;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      onPressed:onTap,
      onLongPress: onLongPress,
      child: GridTile(
          header: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [


              CircularIconButton(
                  height: 35,
                  width: 35,
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                    child: Image.asset(
                      Assets.up,
                      scale: 2,
                      color: colorUp,
                    ),
                  ),
                  onPressed: onUp)
              ,CircularIconButton(
                  height: 35,
                  width: 35,
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                    child: Image.asset(
                      Assets.stop,
                      scale: 2,
                      color: colorStop,
                    ),
                  ),
                  onPressed: onStop),

              CircularIconButton(
                    height: 35,
                    width: 35,
                    color: Theme.of(context).backgroundColor,
                  child: Center(
                    child: Image.asset(
                      Assets.down,
                      scale: 2,
                      color: colorDown,
                    ),
                  ),
                  onPressed: onDwon),
            Text(
                '$name',
                style: Theme.of(context)
                    .textTheme
                    .subtitle2
                    .copyWith(color: Theme.of(context).accentColor),
              ),

    ],
          ),

          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(18.0),
            ),
          )),
    );
  }
}
