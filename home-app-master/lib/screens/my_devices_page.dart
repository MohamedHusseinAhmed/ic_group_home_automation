

import 'package:flutter/material.dart';
import 'package:home_app/components/device_card.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/device.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/devices_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/assets.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:provider/provider.dart';

import 'add_device.dart';
import 'devices_page.dart';

class MyDevicesPage extends StatefulWidget {
  static const route = '/myDevices';
  @override
  _MyDevicesPageState createState() => _MyDevicesPageState();
}

class _MyDevicesPageState extends State<MyDevicesPage> {
  int selected = 0;
  DeviceServices _deviceServices = DeviceServices();
  TextEditingController _name = TextEditingController();
  TextEditingController _group = TextEditingController();
  TextEditingController _zone = TextEditingController();
  TextEditingController _button = TextEditingController();
   List<Device> devices = [];
  CollectionServices _collectionServices = CollectionServices();
  GlobalKey key = GlobalKey();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  getDevices()async{
    return await _deviceServices.getDevices(context);
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: FloatingActionButton(onPressed: (){
      //  Navigator.of(context).pushNamed(AddDevicePage.route);
        // Navigator.of(context).pushNamed(CurtainsPage.route);
        Navigator.pushNamed(context, AddDevicePage.route);
      //  Navigator.pushReplacementNamed(context, AddDevicePage.route);
      },child: Icon(Icons.add,color: Theme.of(context).iconTheme.color,),),

      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Theme.of(context).iconTheme.color,
            onPressed: () {
              Navigator.of(context).pop();
            }),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Lighting',
          style: Theme.of(context).textTheme.headline6.copyWith(color:Colors.white),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child:buildDevices()
              ),
            ),
          ],
        ),
      ),


    );
  }


  Widget buildDevices() {
    return Consumer<DeviceProvider>(
      builder: (context, deviceProvider, child) => FutureBuilder(
        future:  getDevices(),
        builder: (context, snapshot) {

          if (snapshot.hasData) {
            devices = snapshot.data;
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.54,
              child: GridView.builder(
                itemCount: devices.length,

                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: isLarge(context) ? 1.2 : 1.1,
                    crossAxisCount: isLarge(context) ? 5 : 2,
                    crossAxisSpacing: 25,
                    mainAxisSpacing: 10),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Consumer<DeviceProvider>(
                      builder: (context, value, child) =>
                          DeviceCard(
                        name: '${devices[index].name}',
                        status: devices[index].value,
                        color: devices[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,
                        onTap: () {
                          value.setDeviceNumber(index);

                          setState(() {
                            devices[index].value == 'on'
                                ? devices[index].value = 'off'
                                : devices[index].value = 'on';
                          });
                          updateDeviceValue(devices[index].id, Device(value: devices[index].value));

                        },

                            state: devices[index].value == 'on'
                                ? Assets.lampon
                                  : Assets.lampoff ,
                        onLongPress: () {

                          deviceProvider.setDevice(devices[index]);
                          devices[index].name == null ? _name.text =""  :  _name.text =devices[index].name.toString();
                          devices[index].group == null ? _group.text =""  :  _group.text =devices[index].group.toString();
                          devices[index].zone == null ? _zone.text =""  :  _zone.text =devices[index].zone.toString();
                          devices[index].button == null ? _button.text ="" :  _button.text =devices[index].button.toString();

                          showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Container(
                                // height:MediaQuery.of(context).size.height * 0.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  //  mainAxisSize: MainAxisSize.max,
                                  children: [
                                 Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(

                                          controller: _name,
                                          style: TextStyle(

                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Button Name',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _group,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Group ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _group = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _zone,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Zone ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(

                                          controller: _button,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),

                                          decoration: InputDecoration(
                                            hintText: 'Button ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        RaisedButton(
                                          color: Colors.red,
                                          child: const Text('Close'),
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        ),RaisedButton(
                                          color: Colors.red,
                                          child: const Text('Delete'),
                                          onPressed: () {
                                            value.setDeviceNumber(index);
                                            deleteDeviceDetails(index,devices[index].id,
                                                Device(name: _name.text));

                                            Navigator.pop(context);
                                          },
                                        ),
                                        RaisedButton(
                                          color: Theme.of(context).accentColor,
                                          child: const Text('Done'),
                                          onPressed: () {

                                            value.setDeviceNumber(index);
                                            updateDeviceDetails(index,devices[index].id,
                                                Device(name: _name.text,zone:int.tryParse(_zone.text),group:int.tryParse(_group.text),button:int.tryParse(_button.text)) );
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                  );



                },
              ),
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }



void updateDeviceDetails(int index ,String id, Device _device) async {

  if (await _deviceServices.updateDevice(id, _device,context)) {
    setState(() {
      devices[index].name = _device.name;
    });
    print("pin number:Updated");
  } else
    print('error');
}
  void deleteDeviceDetails(int index ,String id, Device _device) async {

    if (await _deviceServices.deleteDevice(id, _device,context)) {
      setState(() {
        devices[index].name = _device.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }


void updateDeviceValue( String id, Device _device) async {

  if (await _deviceServices.updateDevice(id, _device,context)) {
    setState(() {

    });
    print("pin number:Updated");
  } else
    print('error');
}


}