import 'dart:async';
import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_app/Camera/video_1.dart';
import 'package:home_app/Camera/video_2.dart';
import 'package:home_app/Camera/video_intercom_1.dart';
import 'package:home_app/Camera/video_intercom_2.dart';
import 'package:home_app/components/icon_button.dart';
import 'package:home_app/components/sensor_card.dart';
import 'package:home_app/models/collections.dart';
import 'package:home_app/models/sensor_model.dart';
import 'package:home_app/models/weather.dart';
import 'package:home_app/screens/add_device.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/screens/room_page.dart';
import 'package:home_app/services/api/ac.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/auth.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/curtain.dart';
import 'package:home_app/services/api/sensor.dart';
import 'package:home_app/services/mqtt/MQTTAppState.dart';
import 'package:home_app/services/mqtt/MQTTManager.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/acs_provider.dart';
import 'package:home_app/services/provider/curtain_provider.dart';
import 'package:home_app/services/provider/sensors_provider.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:provider/provider.dart';
import 'package:home_app/components/add_room_card.dart';
import 'package:home_app/components/room_card.dart';
import 'package:home_app/screens/add_room.dart';
import 'package:home_app/screens/settings.dart';
import 'package:home_app/services/api/device.dart';
import 'package:home_app/services/api/weather_services.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/devices_provider.dart';
import 'package:home_app/services/provider/user_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/utils/assets.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:ui' as ui show ImageFilter;

import 'package:webview_flutter/webview_flutter.dart';

int _selectedIndex = 0;
int count = 0;
bool plays=false;

class HomePage extends StatefulWidget {
  static const String route = '/home';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CollectionServices _collectionServices = CollectionServices();
  SensorServices _sensorServices = SensorServices();
  bool switchValue = false;
  GlobalKey _globalKey;
  ValueNotifier sensorNumber = ValueNotifier<int>(0);
  bool _switchValue=true;
  List<Sensor> sensors = [];
  bool animate = false;
  DeviceServices _deviceServices = DeviceServices();
  CurtainServices _curtainServices = CurtainServices();
  ACServices _acServices = ACServices();
   WeatherServices weatherServices = WeatherServices();
  Weather weather = Weather();
  AudioCache player = new AudioCache();
  TextEditingController password = TextEditingController();
  AudioPlayer audioPlugin = AudioPlayer();

  MqttServerClient mqttServices ;
  String alarmAudioPath = "sound.mp3";
  getweather() async {
    var data = await weatherServices.fetchData();
  }

  AuthServices authServices = AuthServices();



  @override
  Widget build(BuildContext context) {


    Api.mqttServices.client.updates.listen((
        List<MqttReceivedMessage<MqttMessage>> c) {
      String state = "";
      final MqttPublishMessage message = c[0].payload;
      final payload = MqttPublishPayload.bytesToStringAsString(
          message.payload.message);
      print(  'Received message:$payload from topic: ${c[0].topic.split('/')[1]}>');

      Map<String, dynamic> responseJson = json.decode(payload);


        if (c[0].topic.split('/')[0] == "sensors") {
          setState(() {
          for (int i = 0; i < sensors.length; i++) {
            if (sensors[i].id == c[0].topic.split('/')[1]) {
              sensors[i].value = responseJson["value"];
              sensors[i].army = responseJson["army"];
           //     Provider.of<SensorProvider>(context,listen: false).setSensors(sensors);
            }
          }
          });
        }
    });
    return LayoutBuilder(
      builder: (context, constraints) => Scaffold(

        key: _globalKey,
        appBar: buildAppBar(context),
        backgroundColor: Theme.of(context).backgroundColor,
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 80,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(22.0),
                    bottomLeft: Radius.circular(22.0),
                  ),
                ),
                child: Wrap(
                  direction: Axis.vertical,
                  crossAxisAlignment: WrapCrossAlignment.start,
                  alignment: WrapAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, bottom: 0, left: 10),
                      child: Consumer<UserProvider>(
                        builder: (context, value, child) => Text(
                          'Hello, ${value.user.name} ',
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(color: Colors.white),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 10.0,
                      ),
                      child: Text('Good to see you again',
                          style: Theme.of(context).textTheme.bodyText2),
                    ),

                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.7,
                width: MediaQuery.of(context).size.width,
                child: Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.spaceBetween,
                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: [


                    _buildList(_selectedIndex),
                  ],
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: CurvedNavigationBar(

          items:  [
            Icon(Icons.room_preferences),
            Icon(Icons.add_alert),
            Icon(Icons.camera_alt),
            Icon(Icons.phonelink_ring),
          ],


            onTap: (index) {

    _onItemTapped(index);
    },
          index: _selectedIndex,
          color: Theme.of(context).primaryColor,
          backgroundColor: Theme.of(context).backgroundColor,
          buttonBackgroundColor: Color(0xFFB19A0D),
          animationDuration: Duration(milliseconds: 200),
          height: 50.0,

        ),

      ),
    );
  }
  Future _refreshData() async {

  }
  Widget builder(int current,context)
  {
    if(current ==0) {

      return buildRooms(context);
    }
    else if(current ==1) {

      return  buildSensors(context);
    }
    else if(current ==2) {

      return //VideoPage();

      Container(
        height: MediaQuery.of(context).size.height * 0.7,
        width: MediaQuery.of(context).size.width,
        child: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.spaceBetween,
          crossAxisAlignment: WrapCrossAlignment.start,
          children: [


            VideoPage_1(),VideoPage_2()
          ],
        ),
      );



    }else if(current ==3) {

      return //VideoIntercomPage();

      Container(
        height: MediaQuery.of(context).size.height * 0.7,
        width: MediaQuery.of(context).size.width,
        child: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.spaceBetween,
          crossAxisAlignment: WrapCrossAlignment.start,
          children: [
            VideoIntercomPage_1(),VideoIntercomPage_2()
          ],
        ),
      );



    }

  }
  Widget _buildList(int count) {
    return RefreshIndicator(
      onRefresh: _refreshData,
      child:  builder(count,context)  ,
    );
  }
  void _onItemTapped(int index) {

    setState(() {
      _selectedIndex = index;
    });
  }
  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      actions: [
        Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 12),
          child: RectIconButton(
              height: 32,
              width: 32,
              onPressed: () {
                showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (context) {
                    var width = MediaQuery.of(context).size.width;
                    var height = MediaQuery.of(context).size.height;
                    double buttonSize = 63;
                    return addDialogWidget(context, width, height, buttonSize);
                  },
                );
              },
              color: Colors.white,
              child: Image.asset(
                Assets.menuIcon,
                scale: 2,
              )),
        ),
      ],
      leading: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Container(
          child: CircularIconButton(
            height: 32,
            width: 32,
            color: Colors.white,
            child: Hero(
              tag: 'profile',
              child: Consumer<UserProvider>(
                builder: (context, value, child) => Container(
                  width: 32.0,
                  height: 32.0,
                  child: Icon(Icons.menu_open),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.all(Radius.elliptical(16.0, 16.0)),

                  ),
                ),
              ),
            ),
            onPressed: _setting,
          ),
        ),
      ),
    );
  }

  Widget addDialogWidget(
      BuildContext context, double width, double height, double buttonSize) {
    return ClipRect(
      child: BackdropFilter(
        filter: ui.ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
        child: GestureDetector(
          onTap: () {
            animate = false;
            Navigator.pop(context);
          },
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              color: const Color(0x57ffffff),
            ),
            child: Center(
              child: Stack(
                children: [
                  Center(
                      child: CircularIconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.close),
                    color: Colors.white,
                    height: buttonSize,
                    width: buttonSize,
                  )),
                  Positioned(
                      top: height * 0.5 - (buttonSize * 2),
                      left: width * 0.5 - (buttonSize / 2),
                      child: Center(
                          child: CircularIconButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed(AddDevicePage.route);
                        },
                        child: Center(
                            child: Text(
                          'Device',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(color: AppColors.textColor_light),
                        )),
                        color: Colors.white,
                        height: buttonSize,
                        width: buttonSize,
                      ))),
                  Positioned(
                      top: height * 0.5 - (buttonSize / 2),
                      left: width * 0.5 - (buttonSize * 2),
                      child: Center(
                          child: CircularIconButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed(AddRoomPage.route);
                        },
                        child: Center(
                            child: Text(
                          'Room',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(color: AppColors.textColor_light),
                        )),
                        color: Colors.white,
                        height: buttonSize,
                        width: buttonSize,
                      ))),
                  Positioned(
                      top: height * 0.5 - (buttonSize / 2),
                      right: width * 0.5 - (buttonSize * 2),
                      child: Center(
                          child: CircularIconButton(
                        onPressed: () async {
                          authServices.logOut().then((value) {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        LoginPage()),
                                (Route<dynamic> route) => false);
                          }).catchError((onError) {
                            print(onError);
                          });
                        },
                        child: Center(
                            child: Text(
                          'logOut',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(color: AppColors.textColor_light),
                        )),
                        color: Colors.white,
                        height: buttonSize,
                        width: buttonSize,
                      ))),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildWeather(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      // height: 66.0,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
      ),
      child: Wrap(
        direction: Axis.horizontal,
        alignment: WrapAlignment.spaceBetween,
        crossAxisAlignment: WrapCrossAlignment.end,
        children: [
          Wrap(
            direction: Axis.horizontal,
            crossAxisAlignment: WrapCrossAlignment.center,
            // alignment: WrapAlignment.end,
            children: [
              Consumer<ThemeChanger>(
                builder: (context, value, child) => CircularIconButton(
                  height: 32,
                  width: 32,
                  color: value.darkTheme
                      ? AppColors.iconsColorBackground2_dark
                      : AppColors.iconsColorBackground3_light,
                  child: Image.asset(
                    Assets.temperatureIcon,
                    color: value.darkTheme
                        ? AppColors.iconsColor_dark
                        : AppColors.iconsColorBackground2_dark,
                    scale: 1.5,
                  ),
                  onPressed: null,
                ),
              ),
              Text(
                  'Temperatuer ${weather.temp == null ? '' : weather.temp.value.floor()} C')
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Wrap(
              direction: Axis.horizontal,
              crossAxisAlignment: WrapCrossAlignment.center,
              // alignment: WrapAlignment.end,
              children: [
                Consumer<ThemeChanger>(
                  builder: (context, value, child) => CircularIconButton(
                    height: 32,
                    width: 32,
                    color: value.darkTheme
                        ? AppColors.iconsColorBackground3_dark
                        : AppColors.iconsColorBackground3_light,
                    child: Image.asset(
                      Assets.humidityIcon,
                      color: value.darkTheme
                          ? AppColors.iconsColor_dark
                          : AppColors.iconsColorBackground3_dark,
                      scale: 1.5,
                    ),
                    onPressed: null,
                  ),
                ),
                Text(
                    'Humidity ${weather.humidity == null ? '' : weather.humidity.value.floor()} %')
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRooms(BuildContext context) {


    loadsensors();

    return Consumer<CollectionProvider>(
      builder: (context, value, child) {
        return FutureBuilder(
          future: Provider.of<CollectionProvider>(context).collections,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Collection> rooms = snapshot.data;
              return Container(
                height: MediaQuery.of(context).size.height * 0.42,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: rooms.length + 1,
                  itemBuilder: (context, index) {
                    //*check if index is equal to the last item in the list+1
                    if (index == snapshot.data.length) {
                      return AddRoomCard(
                        onTap: _addRoom,
                      );
                    } else
                      return RoomCard(
                        onPreessed: () {
                          Provider.of<DeviceProvider>(context).setDevices(
                              _deviceServices.getDevicesByIds(
                                  snapshot.data[index].devices,context));
                          Provider.of<CurtainProvider>(context).setCurtains(
                              _curtainServices.getCurtainsByIds(
                                  snapshot.data[index].curtains,context));
                          Provider.of<ACProvider>(context).setACs(
                              _acServices.getACsByIds(
                                  snapshot.data[index].acs,context));
                          Provider.of<CollectionProvider>(context)
                              .setCollection(snapshot.data[index]);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => RoomPage(),
                            ),
                          );
                        },
                        room: rooms[index],
                      );
                  },
                ),
              );
            } else
              return Container(
                height: MediaQuery.of(context).size.height * 0.4,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Theme.of(context).primaryColor,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).backgroundColor),
                  ),
                ),
              );
          },
        );
      },
    );
  }
  _launchURL() async {
     String url = 'http://'+Api.Url+'/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Widget buildcamera() {


  }

  Widget buildSensors(BuildContext context) {
    return Consumer<SensorProvider>(

      builder: (context, sensorProvider, child) => FutureBuilder(

        future:  Provider.of<SensorProvider>(context).sensors,
        builder: (context, snapshot) {

          if (snapshot.hasData) {
            sensors = snapshot.data;
            print(sensors[0].id);
          //  play();
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 100,
              child: GridView.builder(
                itemCount: sensors.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: isLarge(context) ? 1.1 : 1.3,
                    crossAxisCount: isLarge(context) ? 4 : 2,
                    crossAxisSpacing: 25,
                    mainAxisSpacing: 10),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Consumer<SensorProvider>(
                      builder: (context, value, child) => SensorCard(
                        name: '${sensors[index].name}',
                        status: sensors[index].value,
                        state: sensors[index].value == 'on'
                            ? Assets.alert
                            : Assets.safe,
                        sswitch: Container(),
                      /*onTap: () {
                   value.setSensorNumber(index);

                     setState(() {
                        sensors[index].value == 'on'
                       ?sensors[index].value = 'off'
                       :sensors[index].value = 'on';
                    });
                     updateSensorValue(sensors[index].id, Sensor(value: sensors[index].value));
                    },*/

                        color: sensors[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,

                        //  Assets.stop
                        asset:sensors[index].name == 'gas'
                            ? Assets.gas
                            :   sensors[index].name == 'motion'
                              ? Assets.motion : sensors[index].name == 'fire'
                            ? Assets.fire :  sensors[index].name == 'touch'
                            ? Assets.touch :sensors[index].name == 'window'
                            ? Assets.window : Assets.fan ,

                      ),
                    ),
                  );
                },
              ),
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }



  Widget buildScences(BuildContext context) {
    return Container(
      height: isLarge(context) ? 150 : 100,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: scences.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.only(right: 8),
          child: MaterialButton(
            padding: const EdgeInsets.all(0),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => SimpleDialog(
                  title: Center(child: Text('Sorry🥺')),
                  children: [
                    Center(
                        child: Text(
                      ' This feature not work at this time',
                      style: Theme.of(context).textTheme.subtitle2,
                    )),
                  ],
                ),
              );
            },
            child: Container(
              width: isLarge(context) ? 300 : 150,
              height: isLarge(context) ? 150 : 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12.0),
                image: DecorationImage(
                  image: AssetImage(scences[index].imagePath),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                    color: scences[index].shadowColor,
                    offset: Offset(0, 0),
                    blurRadius: 6,
                  ),
                ],
              ),
              child: Center(
                  child: Text(
                'Night mode',
                style: Theme.of(context)
                    .textTheme
                    .subtitle2
                    .copyWith(color: Colors.white),
              )),
            ),
          ),
        ),
      ),
    );
  }


  Timer timer;
  @override
  void initState() {


      timer = Timer.periodic(Duration(seconds: 1), (Timer t) =>   this.mounted ? setState(()  {
        count +=3;

      print ("sensors "+ sensors.length.toString());
      int found = 0;
      for(int i =0 ; i < sensors.length;i++)
      {

        if(sensors[i].army=="on"&&sensors[i].value=="on")
        {

          found = 1;
            break;


        }else if(sensors[i].army=="off"){

          found = 2;


        }
      else if(sensors[i].army=="on"){

        found = 3;
        break;

        }
      }
      if(found==1) {

        if(plays==false )
        {
          stop();
          count==0;
          plays=true;
          play();
          password.text='';
          showModalBottomSheet(
            context: context,
            builder: (context) {
              return Container(
                // height:MediaQuery.of(context).size.height * 0.8,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  //  mainAxisSize: MainAxisSize.max,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                          controller: password,
                          style: TextStyle(
                            fontSize: 22,
                            color: AppColors.textColor2_light,
                            fontWeight: FontWeight.w400,
                          ),
                          decoration: InputDecoration(
                            hintText: 'Enter Password',
                            hintStyle: Theme.of(context)
                                .textTheme
                                .subtitle1,
                          )
                        // onSaved: (val) => _zone = val,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceEvenly,
                      children: [

                        RaisedButton(
                          color: Theme.of(context).accentColor,
                          child: const Text('Enter'),
                          onPressed: () {

                            cheak_alert_password( password.text );



                          },
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
          );



        }






      }else if(found==2)
          {
            if(plays==true && found!=3 )
            {
              plays=false;
              stop();
          //    Navigator.pop(context);
            }
            }


        }
     )

 :{ } );
  }


  void updateSensorDetails1(int index ,String id, Sensor _sensor,context) async {

    if (await _sensorServices.updateSensor(id, _sensor,context)) {
      setState(() {
        sensors[index].name = _sensor.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }
 void cheak_alert_password( String password) async {
        if(password=="1111")
          {

          for(int i =0 ; i < sensors.length;i++)
            {
              sensors[i].army="off";
               // updateSensorDetails1(i ,sensors[i].id, Sensor(id: sensors[i].id,army: "off"  ),context);
              updateSensorValue  (sensors[i].id, Sensor(id: sensors[i].id,army: "off"  ,value:  sensors[i].value,name: sensors[i].name));
            }
          Navigator.pop(context);
          }
        else
          {


        }

  }
  void _setting() {
    Navigator.pushNamed(context, SettingsPage.route);
  }

  void _addRoom() {
    Navigator.pushNamed(context, AddRoomPage.route);
  }

  getCollections()async{

    return await _collectionServices.getCollections(context);
  }

  getSensors()async{

    return await _sensorServices.getSensors(context);
  }

  loadsensors()async{
    if(sensors.length==0)
      sensors=  await _sensorServices.getSensors(context);
      else  sensors= await (Provider.of<SensorProvider>(context).sensors) ;


  }
  void updateSensorDetails(Sensor _sensor) async {
    var sensorProvider = Provider.of<SensorProvider>(context);
    if (await _sensorServices.updateSensor(sensorProvider.sensor.id, _sensor,context)) {
      setState(() {
        // sensors[sensorProvider.sensorNumber].name = _sensor.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }



  void updateSensorValue( String id, Sensor _sensor) async {

    setState(() {
      Api.mqttServices.toggleSensor(
          id, Sensor(value: _sensor.value, army: _sensor.army));
    });
    /*
    if (await _sensorServices.updateSensor(id, _sensor,context)) {
      setState(() {

      });
      print("pin number:Updated");
    } else
      print('error');
      */

  }


  Future play() async {
    audioPlugin =await  player.play(alarmAudioPath,volume: 100 );
   plays=true;
  }

// add a isLocal parameter to play a local file
  Future playLocal() async {
    final result = await audioPlugin.play(alarmAudioPath);

  }


  Future pause() async {
    final result = await audioPlugin.pause();

  }

  Future stop() async {
    final result = await audioPlugin.stop();
    if (result == 1) {



    }
  }


}



class Scences {
  String imagePath;
  Color shadowColor;
  Scences({
    this.imagePath,
    this.shadowColor,
  });
}



List<Scences> scences = [
  Scences(imagePath: Assets.nightImage, shadowColor: Color(0xFF1C4276)),
  Scences(imagePath: Assets.morningImage, shadowColor: Color(0xFFD2C6BD)),
  Scences(imagePath: Assets.eveningImage, shadowColor: Color(0xFF7A4547)),
];
