

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:home_app/components/device_card.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/screens/my_acs_page.dart';
import 'package:home_app/screens/my_curtain_page.dart';
import 'package:home_app/services/api/device.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/devices_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:provider/provider.dart';

import 'add_device.dart';
import 'devices_page.dart';
import 'my_devices_page.dart';

class MyAllDevices extends StatefulWidget {
  static const route = '/myDevices';
  @override
  _MyAllDevices createState() => _MyAllDevices();
}

class _MyAllDevices extends State<MyAllDevices> {
  int selected = 0;



  int _selectedIndex = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child:select_page(_selectedIndex)
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        items:  [
          Icon(Icons.lightbulb),
          Icon(Icons.sensor_window_outlined),
          Icon(Icons.ac_unit_rounded),
        ],
        onTap: (index) {
          _onItemTapped(index);
        },
        index: _selectedIndex,

        color: Theme.of(context).primaryColor,
        backgroundColor: Theme.of(context).backgroundColor,
        buttonBackgroundColor: Color(0xFFB19A0D),
        animationDuration: Duration(milliseconds: 200),
        height: 50.0,

      ),

    );
  }


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  Widget select_page(int c)
  {
    if(c==0)
      {
      return  MyDevicesPage();
      }
    else
    if(c==1)
    {
      return  MyCurtainsPage();

    }else if(c==2)
    {
      return MyACsPage();
    }


  }




}