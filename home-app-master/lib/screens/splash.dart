import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
          color: Theme.of(context).backgroundColor,
          child: Center(
            child: Text('IC Group',style: Theme.of(context).textTheme.headline2,),
          ),
        ));
  }
}
