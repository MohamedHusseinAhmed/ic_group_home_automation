import 'package:flutter/material.dart';
import 'package:home_app/components/curtain_card.dart';

import 'package:home_app/models/curtain_model.dart';
import 'package:home_app/services/api/collection.dart';

import 'package:home_app/services/api/curtain.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/curtain_provider.dart';
import 'package:home_app/theme/color.dart';

import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:provider/provider.dart';

import 'add_curtain.dart';

class MyCurtainsPage extends StatefulWidget {
  static const route = '/myCurtains';
  @override
  _MyCurtainsPageState createState() => _MyCurtainsPageState();
}

class _MyCurtainsPageState extends State<MyCurtainsPage> {
  int selected = 0;
  CurtainServices _curtainServices = CurtainServices();
  TextEditingController _name = TextEditingController();
  TextEditingController _group = TextEditingController();
  TextEditingController _zone = TextEditingController();
  TextEditingController _button = TextEditingController();
  List<Curtain> curtains = [];
  CollectionServices _collectionServices = CollectionServices();
  GlobalKey key = GlobalKey();
  
 getCurtains()async{
    return await _curtainServices.getCurtains(context);
}
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: FloatingActionButton(onPressed: (){

        Navigator.pushNamed(context, AddCurtainPage.route);
      },child: Icon(Icons.add,color: Theme.of(context).iconTheme.color,),),

      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Theme.of(context).iconTheme.color,
            onPressed: () {
              Navigator.of(context).pop();
            }),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Curtains',
          style: Theme.of(context).textTheme.headline6.copyWith(color:Colors.white),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child:buildCurtains()
              ),
            ),
          ],
        ),
      ),
    );
  }
//Todo:fix when resize browser in desktop Futurebuilder called many times

  Widget buildCurtains() {
    return Consumer<CurtainProvider>(
      builder: (context, curtainProvider, child) => FutureBuilder(
        future: getCurtains(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            curtains = snapshot.data;
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.54,
              child: GridView.builder(
                itemCount: curtains.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: isLarge(context) ? 0.9 : 0.9,
                    crossAxisCount: isLarge(context) ? 4 : 2,
                    crossAxisSpacing: 25,
                    mainAxisSpacing: 10),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Consumer<CurtainProvider>(
                      builder: (context, value, child) => CurtainCard(
                        name: '${curtains[index].name}',
                        status: curtains[index].value,

                        colorStop: curtains[index].value != 'stop'
                            ? Theme.of(context).accentColor
                            : Colors.red,
                        colorDown: curtains[index].value != 'down'
                          ? Theme.of(context).accentColor
                          : Colors.red, colorUp: curtains[index].value != 'up'
                          ? Theme.of(context).accentColor
                          : Colors.red,



                        onDwon:  () {
                          value.setCurtainNumber(index);
                          setState(() {
                            curtains[index].value == 'down'
                                ? curtains[index].value = 'down'
                                : curtains[index].value = 'down';
                          });
                          updateCurtainValue(curtains[index].id, Curtain(value: curtains[index].value));

                        },
                        onUp: () {
                          value.setCurtainNumber(index);

                          setState(() {
                            curtains[index].value == 'up'
                                ? curtains[index].value = 'up'
                                : curtains[index].value = 'up';
                          });
                          updateCurtainValue(curtains[index].id, Curtain(value: curtains[index].value));

                        },
                        onStop: () {
                          value.setCurtainNumber(index);

                          setState(() {
                            curtains[index].value == 'stop'
                                ? curtains[index].value = 'stop'
                                : curtains[index].value = 'stop';
                          });
                          updateCurtainValue(curtains[index].id, Curtain(value: curtains[index].value));

                        },
                        onLongPress: () {
                          curtains[index].name == null ? _name.text=""  :  _name.text =curtains[index].name.toString();
                          curtains[index].group == null ? _group.text=""  :  _group.text =curtains[index].group.toString();
                          curtains[index].zone == null ? _zone.text="" :  _zone.text =curtains[index].zone.toString();
                          curtains[index].button == null ? _button.text="":  _button.text =curtains[index].button.toString();

                          curtainProvider.setCurtain(curtains[index]);
                          showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Container(
                                // height:MediaQuery.of(context).size.height * 0.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  //  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _name,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Button Name',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _group,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Group ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _group = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _zone,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Zone ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _button,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Button ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        RaisedButton(
                                          color: Colors.red,
                                          child: const Text('Close'),
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        ),RaisedButton(
                                          color: Colors.red,
                                          child: const Text('Delete'),
                                          onPressed: () {
                                            value.setCurtainNumber(index);
                                            deleteCurtainDetails(index,curtains[index].id,
                                                Curtain(name: _name.text));

                                            Navigator.pop(context);
                                          },
                                        ),
                                        RaisedButton(
                                          color: Theme.of(context).accentColor,
                                          child: const Text('Done'),
                                          onPressed: () {
                                            value.setCurtainNumber(index);
                                            updateCurtainDetails(index,curtains[index].id,
                                                Curtain(name: _name.text,zone:int.tryParse(_zone.text),group:int.tryParse(_group.text),button:int.tryParse(_button.text)) );
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                  );
                },
              ),
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }

  void updateCurtainDetails(int index ,String id, Curtain _Curtain) async {
    var curtainProvider = Provider.of<CurtainProvider>(context);
    if (await _curtainServices.updateCurtain(id, _Curtain,context)) {
      setState(() {
        curtains[index].name = _Curtain.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }
  void deleteCurtainDetails(int index ,String id, Curtain _curtain) async {

    if (await _curtainServices.deleteCurtain(id, _curtain,context)) {
      setState(() {
        curtains[index].name = _curtain.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }
  void updateCurtainValue( String id, Curtain _Curtain) async {

    if (await _curtainServices.updateCurtain(id, _Curtain,context)) {
      setState(() {

      });
      print("pin number:Updated");
    } else
      print('error');
  }


}
