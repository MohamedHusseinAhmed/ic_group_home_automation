import 'package:flutter/material.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:home_app/components/show_loading.dart';
import 'package:home_app/main.dart';
import 'package:home_app/screens/home_page.dart';
import 'package:home_app/services/ap_mode/add_ac_services.dart';

import 'package:home_app/theme/theme_changer.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'my_acs_page.dart';


///Add new AC with AP mode
class IPCONFIG extends StatefulWidget {
  static const String route = '/ip';
  @override
  _IPPageState createState() => _IPPageState();
}

class _IPPageState extends State<IPCONFIG> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _portController = TextEditingController();
  SharedPreferences sharedPreferences;
  final _formKey = GlobalKey<FormState>();
  APModeServices _apModeServices = APModeServices();

  @override
  Widget build(BuildContext context) {

   load();
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: Theme.of(context).iconTheme,
        elevation: 0,
        title: Text(
          'IP Configuration',
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(color: Colors.white),
        ),
        leading: Padding(
          padding: const EdgeInsets.all(12.0),
          child: MaterialButton(
            onPressed: () => Navigator.pop(context),
            padding: EdgeInsets.all(0),
            minWidth: 32,
            height: 32,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(16.0, 16.0)),
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 10,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      color: Theme.of(context).cardColor,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: Theme.of(context).cardColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: form(),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: MaterialButton(
        child: Text(
          'Save',
          style: Theme.of(context).textTheme.headline6,
        ),
        height: 45,
        minWidth: 120,
        elevation: 0,
        color: Color(0xFFB19A0D),
        onPressed: sendData,
      ),
    );
  }

  Widget form() {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: _nameController,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
              ),
              decoration: InputDecoration(
                hintText: 'ip',

                hintStyle: Theme.of(context).textTheme.subtitle1,
              ),

            ), TextFormField(
              controller: _portController,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
              ),
              decoration: InputDecoration(
                hintText: 'Port',

                hintStyle: Theme.of(context).textTheme.subtitle1,
              ),

            ),

          ],
        ));
  }


  sendData() async {

        SharedPreferences myPrefs = await SharedPreferences.getInstance();
        myPrefs.setString('ip',_nameController.text);
        myPrefs.setString('port',_portController.text);
        FlutterRestart.restartApp();
  }



  load()async{
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    final String ip = myPrefs.getString('ip');
    final String port = myPrefs.getString('port');
    _nameController.text=ip;
    _portController.text=port;


  }
  showError() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        actions: [
          RaisedButton(
            onPressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: Text('Ok'),
          )
        ],
        title: Center(
          child: Text(
            'Error',
          ),
        ),
        content: Text(
            'Oh no!🤦‍♂️ can\'t add AC , please make sure you connect to AC'),
        contentTextStyle: TextStyle(color: Colors.black),
      ),
    );
  }
}
