

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_app/components/sensor_card.dart';
import 'package:home_app/models/sensor_model.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/sensor.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/sensors_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/assets.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:provider/provider.dart';

import 'add_sensor.dart';
import 'sensors_page.dart';

class MySensorsPage extends StatefulWidget {
  static const route = '/mySensors';
  @override
  _MySensorsPageState createState() => _MySensorsPageState();
}

class _MySensorsPageState extends State<MySensorsPage> {
  int selected = 0;
  SensorServices _sensorServices = SensorServices();
  TextEditingController _name = TextEditingController();
  TextEditingController _group = TextEditingController();
  TextEditingController _zone = TextEditingController();
  TextEditingController _button = TextEditingController();
  List<Sensor> sensors = [];
  bool _switchValue=true;
  CollectionServices _collectionServices = CollectionServices();
  GlobalKey key = GlobalKey();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  getSensors()async{
    return await _sensorServices.getSensors(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: FloatingActionButton(onPressed: (){
        //  Navigator.of(context).pushNamed(AddSensorPage.route);
        // Navigator.of(context).pushNamed(CurtainsPage.route);
        Navigator.pushNamed(context, AddSensorPage.route);
        //  Navigator.pushReplacementNamed(context, AddSensorPage.route);
      },child: Icon(Icons.add,color: Theme.of(context).iconTheme.color,),),

      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Theme.of(context).iconTheme.color,
            onPressed: () {
              Navigator.of(context).pop();
            }),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Emergency Sensors',
          style: Theme.of(context).textTheme.headline6.copyWith(color:Colors.white),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child:buildSensors(context)
              ),
            ),
          ],
        ),
      ),

    );
  }



  Widget buildSensors(BuildContext context) {
    return Consumer<SensorProvider>(

      builder: (context, sensorProvider, child) => FutureBuilder(

        future:getSensors(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            sensors = snapshot.data;
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 100,
              child: GridView.builder(
                itemCount: sensors.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: isLarge(context) ? 0.9 : 0.9,
                    crossAxisCount: isLarge(context) ? 4 : 2,
                    crossAxisSpacing: 25,
                    mainAxisSpacing: 10),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Consumer<SensorProvider>(
                      builder: (context, value, child) => SensorCard(
                        name: '${sensors[index].name}',
                        status: sensors[index].value,
                        state: sensors[index].value == 'on'
                            ? Assets.alert
                            : Assets.safe,

                        color: sensors[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,
                        sswitch:Container(  child: CupertinoSwitch(
                          value: sensors[index].army== 'on' ? true : false,
                          onChanged: (value) {
                            setState(() {
                              _switchValue = value;
                              sensors[index].army= value == true ? 'on' : 'off';
                              //updateSensorDetails(index,sensors[index].id , Sensor(army:value == true ? 'on' : 'off', ),context);
                              updateSensorValue( sensors[index].id , Sensor(    value: sensors[index].value, army:value == true ? 'on' : 'off', ),context);
                            });
                          },
                        ),),
                        //  Assets.stop
                        asset:sensors[index].name == 'gas'
                            ? Assets.gas
                            :   sensors[index].name == 'motion'
                            ? Assets.motion : sensors[index].name == 'fire'
                            ? Assets.fire :  sensors[index].name == 'touch'
                            ? Assets.touch :sensors[index].name == 'window'
                            ? Assets.window : Assets.fan ,

                      ),
                    ),
                  );
                },
              ),
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }



  void updateSensorDetails(int index ,String id, Sensor _sensor,context) async {

    if (await _sensorServices.updateSensor(id, _sensor,context)) {
      setState(() {
        sensors[index].name = _sensor.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }
  void deleteSensorDetails(int index ,String id, Sensor _sensor,context) async {

    if (await _sensorServices.deleteSensor(id, _sensor,context)) {
      setState(() {
        sensors[index].name = _sensor.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }


  void updateSensorValue( String id, Sensor _sensor,context) async {

    setState(() {
      Api.mqttServices.toggleSensor(
          id, Sensor(value: _sensor.value, army: _sensor.army));
    });
    /*
    if (await _sensorServices.updateSensor(id, _sensor,context)) {
      setState(() {

      });
      print("pin number:Updated");
    } else
      print('error');
      */

  }


}