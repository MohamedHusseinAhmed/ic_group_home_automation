import 'package:flutter/material.dart';
import 'package:home_app/components/show_loading.dart';
import 'package:home_app/main.dart';
import 'package:home_app/screens/home_page.dart';
import 'package:home_app/services/ap_mode/add_ac_services.dart';
import 'package:home_app/services/api/auth.dart';

import 'package:home_app/theme/theme_changer.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'My_all_devices.dart';
import 'login_screen.dart';
import 'my_acs_page.dart';


///Add new AC with AP mode
class AddACPage extends StatefulWidget {
  static const String route = '/addAC';
  @override
  _AddACPageState createState() => _AddACPageState();
}

class _AddACPageState extends State<AddACPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  SharedPreferences sharedPreferences;
  final _formKey = GlobalKey<FormState>();
  APModeServices _apModeServices = APModeServices();
  AuthServices authServices = AuthServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: Theme.of(context).iconTheme,
        elevation: 0,
        title: Text(
          'Add ac',
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(color: Colors.white),
        ),
        leading: Padding(
          padding: const EdgeInsets.all(12.0),
          child: MaterialButton(
            onPressed: () => Navigator.pop(context),
            padding: EdgeInsets.all(0),
            minWidth: 32,
            height: 32,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(16.0, 16.0)),
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 10,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      color: Theme.of(context).cardColor,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 150,
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: Theme.of(context).cardColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: form(),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: MaterialButton(
        child: Text(
          'Next',
          style: Theme.of(context).textTheme.headline6,
        ),
        height: 45,
        minWidth: 120,
        elevation: 0,
        color: Theme.of(context).accentColor,
        onPressed: sendData,
      ),
    );
  }

  Widget form() {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: _nameController,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
              ),
              decoration: InputDecoration(
                hintText: 'A/C Name',
                hintStyle: Theme.of(context).textTheme.subtitle1,
              ),

            ),

          ],
        ));
  }


  sendData() async {
    try {
      //TODO:if not connect to server after 10 s;
      // showLoading(context);
      var res = await _apModeServices.sendData(
          _nameController.text);
      if (res != null) {
        if (res=="200") {
          Navigator.pushNamed(context, MyAllDevices.route);
        }else if (res=="401") {
          authServices.logOut().then((value) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()),
                    (Route<dynamic> route) => false);
          }).catchError((onError) {
            print(onError);
          });
        } else {
          showError();
        }
      }
    } catch (e) {
      showError();
    }

  }


  showError() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        actions: [
          RaisedButton(
            onPressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: Text('Ok'),
          )
        ],
        title: Center(
          child: Text(
            'Error',
          ),
        ),
        content: Text(
            'Oh no!🤦‍♂️ can\'t add AC , please make sure you connect to AC'),
        contentTextStyle: TextStyle(color: Colors.black),
      ),
    );
  }
}
