import 'package:flutter/material.dart';
import 'package:home_app/components/list_curtain_card.dart';
import 'package:home_app/components/show_loading.dart';
import 'package:home_app/models/curtain_model.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/curtain.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/curtain_provider.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/assets.dart';
import 'package:provider/provider.dart';

class CurtainsPage extends StatefulWidget {
  static const route = '/curtains';
  @override
  _CurtainsPageState createState() => _CurtainsPageState();
}

class _CurtainsPageState extends State<CurtainsPage> {
  int selected = 0;
  CurtainServices _curtainServices = CurtainServices();
  CollectionServices _collectionServices = CollectionServices();
  // GlobalKey _scaffoldKey = GlobalKey();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(12.0),
          child: MaterialButton(
            onPressed: () => Navigator.pop(context),
            padding: EdgeInsets.all(0),
            minWidth: 32,
            height: 32,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(16.0, 16.0)),
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
        ),
        elevation: 0,
        title: Consumer<CollectionProvider>(
          builder: (context, value, child) =>
              Text(
                '${value.collection.name}',
                style: Theme.of(context).textTheme.headline6.copyWith(color:Colors.white),
                textAlign: TextAlign.left,
              ),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: FutureBuilder(
                  future: _curtainServices.getCurtains( context),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<Curtain> curtains = snapshot.data;
                      return ListView.builder(
                        shrinkWrap: false,
                        scrollDirection: Axis.vertical,
                        itemCount: curtains.length,
                        itemBuilder: (context, index) {
                          return ListCurtainCard(
                            icon: Container(
                              height: 32,
                              width: 32,
                              decoration: BoxDecoration(
                                color: Theme.of(context).cardColor,
                                borderRadius: BorderRadius.circular(22),
                              ),
                              child: Center(
                                child: Image.asset(
                                  Assets.deviceIcon,
                                  scale: 1.5,
                                  color:
                                  Theme.of(context).iconTheme.color,
                                ),
                              ),
                            ),
                            title: curtains[index].name ?? 'No name',
                            onTap: () {
                              addSelectetdCurtain(curtains[index]);
                            },
                            // icon:,
                          );
                        },
                      );
                    } else
                      return Container();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  addSelectetdCurtain(Curtain curtain) async {
    var collection = Provider.of<CollectionProvider>(context).collection;
    showLoading(context);
    var res = await _collectionServices.addCurtainToCollection(
        collection.id, curtain.id,context);
    if (res != null) {
      stopLoading();
      if (res) {
        collection.curtains.add(curtain.id);
        var curtains = await Provider.of<CurtainProvider>(context).curtains;
        curtains.add(curtain);
        Future<List<Curtain>> devs() async {
          var devs = curtains;
          return devs;
        }
        Provider.of<CurtainProvider>(context).setCurtains(devs());
        Provider.of<CollectionProvider>(context).setCollection(collection);
      } else {
        final snackBar = SnackBar(content: Text('Curtain already there!!'));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } else {
      errorDialog(collection.name, curtain.name);
    }
  }

  stopLoading() {
    Navigator.pop(context);
  }


  errorDialog(String collectionName, String curtainName) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Center(
          child: Text(
            'Error',
          ),
        ),
        content: Text(
            'Oh no!🤦‍♂️ can\'t add $curtainName to $collectionName now,\ncheck your connection or try later'),
        contentTextStyle: TextStyle(color: Colors.black),
      ),
    );
  }
}
