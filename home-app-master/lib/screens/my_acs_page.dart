

import 'package:flutter/material.dart';
import 'package:home_app/components/AC_card.dart';
import 'package:home_app/components/device_card.dart';
import 'package:home_app/models/ac_model.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/services/api/ac.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/device.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/acs_provider.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/devices_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:provider/provider.dart';

import 'add_ac.dart';
import 'add_device.dart';
import 'devices_page.dart';

class MyACsPage extends StatefulWidget {
  static const route = '/myACs';
  @override
  _MyACsPageState createState() => _MyACsPageState();
}

class _MyACsPageState extends State<MyACsPage> {
  int selected = 0;
  ACServices _acServices = ACServices();
  TextEditingController _name = TextEditingController();
  TextEditingController _group = TextEditingController();
  TextEditingController _zone = TextEditingController();
  TextEditingController _button = TextEditingController();
   List<AC> acs = [];
  CollectionServices _collectionServices = CollectionServices();
  double values=0;
  GlobalKey key = GlobalKey();
  
 getACs()async{
    return await _acServices.getACs(context);
}
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: FloatingActionButton(onPressed: (){
      //  Navigator.of(context).pushNamed(AddACPage.route);
        // Navigator.of(context).pushNamed(CurtainsPage.route);

        Navigator.pushNamed(context,AddACPage.route);

      },child: Icon(Icons.add,color: Theme.of(context).iconTheme.color,),),

      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Theme.of(context).iconTheme.color,
            onPressed: () {
              Navigator.of(context).pop();
            }),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'A/C',
          style: Theme.of(context).textTheme.headline6.copyWith(color:Colors.white),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child:buildACs()
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget buildACs() {
    return Consumer<ACProvider>(
      builder: (context, acProvider, child) => FutureBuilder(
        future: getACs(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            acs = snapshot.data;


            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 200,
              child: GridView.builder(
                itemCount: acs.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: isLarge(context) ? 1.1 : 1.1,
                    crossAxisCount: isLarge(context) ? 2 : 1,
                    crossAxisSpacing: 25,
                    mainAxisSpacing: 10),
                itemBuilder: (context, index) {


                  return Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Consumer<ACProvider>(
                      builder: (context, value, child) => ACCard(

                        onremove:() {  setState(() {
                          if(  acs[index].targetTemp!=null)
                            acs[index].targetTemp--;
                          else acs[index].targetTemp=0;
                          updateACValue(acs[index].id, AC(targetTemp:    acs[index].targetTemp));
                        }); } ,


                        onadd:() {  setState(() {
                          if(  acs[index].targetTemp!=null)
                            acs[index].targetTemp++;
                          else acs[index].targetTemp=0;
                          updateACValue(acs[index].id, AC(targetTemp:    acs[index].targetTemp));

                        }); } ,

                        fanup: () {  setState(() {
                          if(  acs[index].fanSpeed!=null)
                            acs[index].fanSpeed++;
                          else acs[index].fanSpeed=0;
                          updateACValue(acs[index].id, AC(fanSpeed:    acs[index].fanSpeed));

                        }); }
                        ,
                        fanIcon:  acs[index].fanSpeed == 1
                            ?  Icon(  Icons.looks_one,color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,  )
                            : acs[index].fanSpeed == 2
                            ?  Icon(  Icons.looks_two, color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red, ):
                        acs[index].fanSpeed == 3
                            ?  Icon(  Icons.threed_rotation_rounded, color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red, ):
                        acs[index].fanSpeed == 5
                            ?  Icon(  Icons.autorenew_outlined,color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,  ):
                        Icon(  Icons.error_outline_rounded, color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red, ),



                        onMode: () {
                          value.setACNumber(index);
                          if( acs[index].mode == "heat")
                          {
                            acs[index].mode = "cool";
                          }else if( acs[index].mode == "cool")
                          {
                            acs[index].mode = "fan";
                          }else if( acs[index].mode == "fan")
                          {
                            acs[index].mode = "heat";
                          }

                          updateACValue(acs[index].id, AC(mode:  acs[index].mode));

                        }  ,
                        fandown: () {  setState(() {
                          if(  acs[index].fanSpeed!=null)
                            acs[index].fanSpeed--;
                          else acs[index].fanSpeed=0;
                          updateACValue(acs[index].id, AC(fanSpeed:    acs[index].fanSpeed));

                        }); },
                        ModeIcon: acs[index].mode == 'heat'
                            ?  Icon(  Icons.wb_sunny_outlined, color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red, )
                            : acs[index].mode == 'cool'
                            ?  Icon(  Icons.ac_unit,color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,  )
                            : acs[index].mode == 'fan'
                            ?  Icon(  Icons.flip_camera_android_outlined,color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,  )
                            : Icon(  Icons.error_outline_rounded, color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red, )    ,

                        name: '${acs[index].name}',
                        status: acs[index].value,
                        color: acs[index].value == 'on'
                            ? Theme.of(context).accentColor
                            : Colors.red,
                        TargetT: acs[index].targetTemp,

                        currentT: acs[index].currentTemp
                        ,


                        onPower: () {
                          value.setACNumber(index);

                          acs[index].value == 'on'
                              ? acs[index].value = 'off'
                              : acs[index].value = 'on';
                          updateACValue(acs[index].id, AC(value:  acs[index].value));

                        } ,
                        onFan:  () {
                          value.setACNumber(index);
                          if( acs[index].fanSpeed == 1)
                          {
                            acs[index].fanSpeed = 2;
                          }else if( acs[index].fanSpeed == 2)
                          {
                            acs[index].fanSpeed = 3;
                          }else if( acs[index].fanSpeed == 3)
                          {
                            acs[index].fanSpeed = 5;
                          }else {
                            acs[index].fanSpeed = 1;}

                          updateACValue(acs[index].id, AC(fanSpeed: acs[index].fanSpeed));

                        }   ,

                        onLongPress: () {
                          acProvider.setAC(acs[index]);

                          acs[index].name == null ? _name.text =""  :  _name.text =acs[index].name.toString();
                          acs[index].group == null ? _group.text =""  :  _group.text =acs[index].group.toString();

                          showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Container(
                                // height:MediaQuery.of(context).size.height * 0.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  //  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _name,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'Name',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _group = val,
                                      ),
                                    ), Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextField(
                                          controller: _group,
                                          style: TextStyle(
                                            fontSize: 22,
                                            color: AppColors.accentColor_dark,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          decoration: InputDecoration(
                                            hintText: 'ID',
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          )
                                        // onSaved: (val) => _zone = val,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        RaisedButton(
                                          color: Colors.red,
                                          child: const Text('Close'),
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        ),RaisedButton(
                                          color: Colors.red,
                                          child: const Text('Delete'),
                                          onPressed: () {
                                            value.setACNumber(index);
                                            deleteACDetails(index,acs[index].id,
                                                AC(name: _name.text));

                                            Navigator.pop(context);
                                          },
                                        ),
                                        RaisedButton(
                                          color: Theme.of(context).accentColor,
                                          child: const Text('Done'),
                                          onPressed: () {

                                            value.setACNumber(index);
                                            updateACDetails(  index,acs[index].id,   AC(group: int.tryParse(_group.text),name:   _name.text  ));
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                  );
                },
              ),
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }



  void updateACDetails(int index ,String id, AC _ac) async {

  if (await _acServices.updateAC(id, _ac,context)) {
    setState(() {
      acs[index].name = _ac.name;
    });
    print("pin number:Updated");
  } else
    print('error');
}
void deleteACDetails(int index ,String id, AC _ac) async {

    if (await _acServices.deleteAC(id, _ac,context)) {
      setState(() {
        acs[index].name = _ac.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }


void updateACValue( String id, AC _ac) async {

  if (await _acServices.updateAC(id, _ac,context)) {
    setState(() {

    });
    print("pin number:Updated");
  } else
    print('error');
}






}
