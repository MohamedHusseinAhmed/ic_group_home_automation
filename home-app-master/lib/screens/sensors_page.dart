import 'package:flutter/material.dart';
import 'package:home_app/components/list_sensor_card.dart';
import 'package:home_app/components/show_loading.dart';
import 'package:home_app/models/sensor_model.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/sensor.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/sensors_provider.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/assets.dart';
import 'package:provider/provider.dart';

class SensorsPage extends StatefulWidget {
  static const route = '/sensors';
  @override
  _SensorsPageState createState() => _SensorsPageState();
}

class _SensorsPageState extends State<SensorsPage> {
  int selected = 0;
  SensorServices _sensorServices = SensorServices();
  CollectionServices _collectionServices = CollectionServices();
  // GlobalKey _scaffoldKey = GlobalKey();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(12.0),
          child: MaterialButton(
            onPressed: () => Navigator.pop(context),
            padding: EdgeInsets.all(0),
            minWidth: 32,
            height: 32,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(16.0, 16.0)),
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
        ),
        elevation: 0,
        title: Consumer<CollectionProvider>(
          builder: (context, value, child) =>
              Text(
                '${value.collection.name}',
                style: Theme.of(context).textTheme.headline6.copyWith(color:Colors.white),
                textAlign: TextAlign.left,
              ),
        ),
      ),
      body: Consumer<ThemeChanger>(
        builder: (context, theme, child) => Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: FutureBuilder(
                  future: _sensorServices.getSensors(context),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<Sensor> sensors = snapshot.data;
                      return ListView.builder(
                        shrinkWrap: false,
                        scrollDirection: Axis.vertical,
                        itemCount: sensors.length,
                        itemBuilder: (context, index) {
                          return ListSensorCard(
                            icon: Container(
                              height: 32,
                              width: 32,
                              decoration: BoxDecoration(
                                color: Theme.of(context).cardColor,
                                borderRadius: BorderRadius.circular(22),
                              ),
                              child: Center(
                                child: Image.asset(
                                  Assets.deviceIcon,
                                  scale: 1.5,
                                  color:
                                  Theme.of(context).iconTheme.color,
                                ),
                              ),
                            ),
                            title: sensors[index].name ?? 'No name',

                            // icon:,
                          );
                        },
                      );
                    } else
                      return Container();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  stopLoading() {
    Navigator.pop(context);
  }


  errorDialog(String collectionName, String sensorName) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Center(
          child: Text(
            'Error',
          ),
        ),
        content: Text(
            'Oh no!🤦‍♂️ can\'t add $sensorName to $collectionName now,\ncheck your connection or try later'),
        contentTextStyle: TextStyle(color: Colors.black),
      ),
    );
  }
}
