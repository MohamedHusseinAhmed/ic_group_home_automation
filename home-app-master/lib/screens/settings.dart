import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_app/components/icon_button.dart';
import 'package:home_app/components/setting_card.dart';
import 'package:home_app/screens/about_page.dart';
import 'package:home_app/screens/add_device.dart';
import 'package:home_app/screens/login_screen.dart';
import 'package:home_app/screens/my_devices_page.dart';
import 'package:home_app/screens/my_sensors_page.dart';
import 'package:home_app/screens/rooms_page.dart';
import 'package:home_app/screens/profile_edit_page.dart';
import 'package:home_app/services/api/auth.dart';
import 'package:home_app/services/provider/user_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/assets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'My_all_devices.dart';
import 'add_curtain.dart';
import 'add_device_page2.dart';
import 'add_sensor.dart';
import 'ip_config.dart';
import 'my_acs_page.dart';
import 'my_curtain_page.dart';

class SettingsPage extends StatefulWidget {
  static const String route = '/settings';
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  SharedPreferences sharedPreferences;
  double _imageScale = 1.5;

AuthServices authServices = AuthServices();
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<UserProvider>(context, listen: true).user;

    ThemeChanger _themeChange = Provider.of<ThemeChanger>(context);
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: buildAppBar(context),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 10,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(22.0),
                  bottomLeft: Radius.circular(22.0),
                ),
                color: Theme.of(context).primaryColor,
              ),
              child: Wrap(
                direction: Axis.vertical,
                crossAxisAlignment: WrapCrossAlignment.center,
                alignment: WrapAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        direction: Axis.vertical,
                        children: [
                          Hero(
                            tag: 'profile',
                            transitionOnUserGestures: false,
                            child: Container(
                              width: 90.0,
                              height: 90.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.elliptical(45.0, 45.0)),

                              ),
                            ),
                          ),


                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            SettingCard(
              icon: Image.asset(
                Assets.moonIcon,
                scale: _imageScale,
              ),
              title: Provider.of<ThemeChanger>(context).darkTheme
                  ? 'Light mode'
                  : 'Dark mode',
              color: Colors.white,
              trailing: Container(
                margin: EdgeInsets.only(bottom: 22),
                child: Switch(
                  value: _themeChange.darkTheme??false,
                  onChanged: (value) {
                    _themeChange.toggleTheme();
                  },
                  activeColor: Colors.white,
                  activeTrackColor: Theme.of(context).accentColor,
                  // inactiveTrackColor: AppColors.backgroundColor_dark,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
              ),
            ),
            SettingCard(
              icon: Image.asset(
                Assets.deviceIcon,
                scale: _imageScale,
              ),
              title: 'My All Devices',
              onTap: () {
                Navigator.pushNamed(context, MyAllDevices.route);
              },
              color: Colors.white,
              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            SettingCard(
              icon: Image.asset(
                Assets.homeIcon,
                scale: _imageScale,
              ),
              title: 'My rooms',
              color: Colors.white,
              onTap: () {
                Navigator.pushNamed(context, RoomsPage.route);
              },
              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ),SettingCard(
              icon: Image.asset(
                Assets.homeIcon,
                scale: _imageScale,
              ),
              title: 'IP Config',
              color: Colors.white,
              onTap: () {
                Navigator.pushNamed(context, IPCONFIG.route);
              },
              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            SettingCard(
              icon: Image.asset(
                Assets.homeIcon,
                scale: _imageScale,
              ),
              title: 'Add Sensor',
              color: Colors.white,
              onTap: () {
                Navigator.pushNamed(context, AddSensorPage.route);
              },
              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            SettingCard(
              onTap: ()  {
                Navigator.pushNamed(context, MySensorsPage.route);

              },
              icon: Image.asset(
                Assets.safe,
                scale: _imageScale,
              ),
              title: 'Army Mode',

              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ), SettingCard(
              onTap: ()  {




                _launchURL();



            //    Navigator.pushNamed(context, MySensorsPage.route);

              },
              icon: Icon(Icons.system_update_alt,
                  color: Theme.of(context).primaryColor),
              title: 'Update',

              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            SettingCard(
              onTap: () async {
                authServices.logOut().then((value) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage()),
                      (Route<dynamic> route) => false);
                }).catchError((onError) {
                  print(onError);
                });
              },
              icon: Image.asset(
                Assets.logoutIcon,
                scale: _imageScale,
                color: Colors.white,
              ),
              title: 'Logout',
              color: Colors.red,
              trailing: Container(
                child: Icon(Icons.keyboard_arrow_right,
                    color: Theme.of(context).primaryColor),
              ),
            ),Text('Version : 1_6_0',
                style: Theme.of(context).textTheme.bodyText1),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                decoration: BoxDecoration(
                    // color: AppColors.primaryColor_dark,
                    // borderRadius: BorderRadius.all(Radius.elliptical(45.0, 45.0)),
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      leading:  Padding(
        padding: const EdgeInsets.all(12.0),
        child: MaterialButton(
            onPressed: () => Navigator.pop(context),
            padding: EdgeInsets.all(0),
            minWidth: 32,
            height: 32,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.elliptical(16.0, 16.0)),
            ),
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
      ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top:12.0,bottom: 12),
            child: Consumer<ThemeChanger>(
            builder: (context, value, child) => 
             RectIconButton(
              height: 28,
              width: 28,
              onPressed: () {
                Navigator.of(context).pushNamed(ProfileEditPage.route);
              },
              color: value.darkTheme
                      ? AppColors.iconsColorBackground2_dark
                      : AppColors.iconsColorBackground3_light,
              child: Icon(
                Icons.edit,
                color:value.darkTheme? AppColors.iconsColor_dark:AppColors.iconsColor_light,
              ),
            ),
        ),
          ),
        ],
    );
  }
  _launchURL() async {
    String url = 'https://play.google.com/store/apps/details?id=com.icgroup.home_app';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}
