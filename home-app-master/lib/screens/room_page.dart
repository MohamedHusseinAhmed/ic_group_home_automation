import 'dart:async';
import 'dart:convert';
import 'dart:ffi';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:home_app/components/AC_card.dart';
import 'package:home_app/components/curtain_card.dart';
import 'package:home_app/components/device_card.dart';
import 'package:home_app/components/icon_button.dart';
import 'package:home_app/models/ac_model.dart';
import 'package:home_app/models/appliance.dart';
import 'package:home_app/models/connectedModel.dart';
import 'package:home_app/models/curtain_model.dart';
import 'package:home_app/models/device_model.dart';
import 'package:home_app/screens/devices_page.dart';
import 'package:home_app/screens/room_edit.dart';
import 'package:home_app/services/api/ac.dart';
import 'package:home_app/services/api/api.dart';
import 'package:home_app/services/api/collection.dart';
import 'package:home_app/services/api/curtain.dart';
import 'package:home_app/services/api/device.dart';
import 'package:home_app/services/mqtt/MQTTManager.dart';
import 'package:home_app/services/mqtt/mqtt_services.dart';
import 'package:home_app/services/provider/acs_provider.dart';
import 'package:home_app/services/provider/collection_provider.dart';
import 'package:home_app/services/provider/curtain_provider.dart';
import 'package:home_app/services/provider/devices_provider.dart';
import 'package:home_app/theme/color.dart';
import 'package:home_app/theme/theme_changer.dart';
import 'package:home_app/utils/assets.dart';
import 'package:home_app/utils/utilities.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:provider/provider.dart';

import 'ac_page.dart';
import 'curtains_page.dart';

class RoomPage extends StatefulWidget {
  static const String route = '/room';
  @override
  _RoomPageState createState() => _RoomPageState();


}
const List<Tab> tabs = <Tab>[
  Tab(text: 'Zeroth'),
  Tab(text: 'First'),
  Tab(text: 'Second'),
];
class _RoomPageState extends State<RoomPage> {
  DeviceServices _deviceServices = DeviceServices();
  ACServices _acServices = ACServices();

  CurtainServices _curtainServices = CurtainServices();
  CollectionServices _collectionServices = CollectionServices();

  double values = 0;
  int _selectedIndex = 0;

  // ApiServices _apiServices = ApiServices();
  MQTTManager _mqttServices  ;

  ValueNotifier deviceNumber = ValueNotifier<int>(0);
  TextEditingController _group = TextEditingController();
  TextEditingController _zone = TextEditingController();
  TextEditingController _button = TextEditingController();
  int ctab;
  List<Device> devices = [];
  List<AC> acs = [];
  List<Curtain> curtains = [];
  bool value = false;

  @override
  Widget build(BuildContext context) {

    Api.mqttServices.client.updates.listen((
        List<MqttReceivedMessage<MqttMessage>> c) {
      String state = "";
      final MqttPublishMessage message = c[0].payload;
      final payload = MqttPublishPayload.bytesToStringAsString(
          message.payload.message);
      print(
          'Received message:$payload from topic: ${c[0].topic.split('/')[1]}>');

      Map<String, dynamic> responseJson = json.decode(payload);


        if (c[0].topic.split('/')[0] == "devices") {
          setState(() {
          for (int i = 0; i < devices.length; i++) {
            if (devices[i].id == c[0].topic.split('/')[1]) {
              devices[i].value = responseJson["value"];
            }
          }
          });
        } else if (c[0].topic.split('/')[0] == "curtains") {
          setState(() {
            for (int i = 0; i < curtains.length; i++) {
            if (curtains[i].id == c[0].topic.split('/')[1]) {
              curtains[i].value = responseJson["value"];
            }
          }   });
        } else if (c[0].topic.split('/')[0] == "acs") {
          setState(() {
            for (int i = 0; i < acs.length; i++) {
              if (acs[i].id == c[0].topic.split('/')[1]) {
                if (responseJson["value"] != null)
                  acs[i].value = responseJson["value"];
                if (responseJson["mode"] != null)
                  acs[i].mode = responseJson["mode"];
                if (responseJson["fanSpeed"] != null)
                  acs[i].fanSpeed = responseJson["fanSpeed"];
                if (responseJson["targetTemp"] != null)
                  acs[i].targetTemp = responseJson["targetTemp"];
                if (responseJson["currentTemp"] != null)
                  acs[i].currentTemp = responseJson["currentTemp"];
              }
            }
          });
        }

    });
    return Scaffold(
      appBar: buildAppBar(context),
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      floatingActionButton: FloatingActionButton(onPressed: () {
        if (ctab == 0) {
          Navigator.of(context).pushNamed(DevicesPage.route);
        } else if (ctab == 1) {
          Navigator.of(context).pushNamed(CurtainsPage.route);
        } else if (ctab == 2) {
          Navigator.of(context).pushNamed(ACsPage.route);
        }
      }, child: Icon(Icons.add, color: Theme
          .of(context)
          .iconTheme
          .color,),),

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(

            width: MediaQuery
                .of(context)
                .size
                .width,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(22.0),
                bottomLeft: Radius.circular(22.0),
              ),
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
            child: Wrap(

              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.start,
              alignment: WrapAlignment.start,
              children: [

                ON_OFF(_selectedIndex)
              ],
            ),
          ),
          Expanded(
            child: Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10, horizontal: 20)
                  ),
                  Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,

                  ),

                  Expanded(
                    child: _buildList(_selectedIndex),
                  ),
                ],
              ),
            ),
          )
        ],

      ),
      bottomNavigationBar: CurvedNavigationBar(

        items:  [
          Icon(Icons.lightbulb),
          Icon(Icons.sensor_window_outlined),
          Icon(Icons.ac_unit_rounded),

        ],

        onTap: (index) {

          _onItemTapped(index);
        },

        index: _selectedIndex,
        color: Theme.of(context).primaryColor,
        backgroundColor: Theme.of(context).backgroundColor,
        buttonBackgroundColor: Color(0xFFB19A0D),
        animationDuration: Duration(milliseconds: 200),
        height: 50.0,

      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Consumer<CollectionProvider>(
        builder: (context, value, child) =>
            Text(
              '${value.collection.name}',
              style: Theme
                  .of(context)
                  .textTheme
                  .headline6
                  .copyWith(color: Colors.white),
              textAlign: TextAlign.left,
            ),
      ),
      elevation: 0,
      leading: Padding(
        padding: const EdgeInsets.all(12.0),
        child: MaterialButton(
          onPressed: () => Navigator.pop(context),
          padding: EdgeInsets.all(0),
          minWidth: 32,
          height: 32,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.elliptical(16.0, 16.0)),
          ),
          child: Center(
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
              size: 20,
            ),
          ),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 12),
          child: RectIconButton(
              height: 28,
              width: 28,
              onPressed: () {
                Navigator.of(context).pushNamed(RoomEdit.route);
              },
              color: Colors.white,
              child: Icon(
                Icons.edit,
                size: 18,
              )),
        ),
      ],
    );
  }

  Widget buildDevices() {
    return Consumer<DeviceProvider>(

      builder: (context, deviceProvider, child) =>
          FutureBuilder(

            future: deviceProvider.devices,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                devices = snapshot.data;
                return Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20, vertical: 0),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height * 100,
                  child: GridView.builder(
                    itemCount: devices.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: isLarge(context) ? 1.1 : 1.1,
                        crossAxisCount: isLarge(context) ? 5 : 2,
                        crossAxisSpacing: 25,
                        mainAxisSpacing: 10),
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Consumer<DeviceProvider>(
                          builder: (context, value, child) =>
                              DeviceCard(
                                name: '${devices[index].name}',
                                status: devices[index].value,
                                color: devices[index].value == 'on'
                                    ? Theme
                                    .of(context)
                                    .accentColor
                                    : Colors.red,
                                onTap: () {
                                  value.setDeviceNumber(index);

                                  setState(() {
                                    devices[index].value == 'on'
                                        ? devices[index].value = 'off'
                                        : devices[index].value = 'on';
                                  });

                                  updateDeviceValue(devices[index].id, devices[index]);
                                  //     _mqttServices.toggleDevice(devices[index].id,
                                  //       (   Device(  value: devices[index].value == 'off' ? 'off' : 'on')));
                                },
                                state: devices[index].value == 'on'
                                    ? Assets.lampon
                                    : Assets.lampoff,
                                onLongPress: () {
                                  deviceProvider.setDevice(devices[index]);
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return Container(
                                        // height:MediaQuery.of(context).size.height * 0.8,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment
                                              .start,
                                          //  mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _group,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Group ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _group = val,
                                              ),
                                            ), Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _zone,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Zone ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _zone = val,
                                              ),
                                            ), Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _button,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Button ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _zone = val,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                              children: [
                                                RaisedButton(
                                                  color: Colors.red,
                                                  child: const Text('Close'),
                                                  onPressed: () =>
                                                      Navigator.pop(context),
                                                ),
                                                RaisedButton(
                                                  color: Theme
                                                      .of(context)
                                                      .accentColor,
                                                  child: const Text('Done'),
                                                  onPressed: () {
                                                    value.setDeviceNumber(
                                                        index);
                                                    updateDeviceDetails(
                                                        Device(
                                                            group: int.tryParse(
                                                                _group.text),
                                                            zone: int.tryParse(
                                                                _zone.text),
                                                            button: int
                                                                .tryParse(
                                                                _button.text)));
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                },
                              ),
                        ),
                      );
                    },
                  ),
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            },
          ),
    );
  }

  Widget buildCurtains() {
    return Consumer<CurtainProvider>(
      builder: (context, curtainProvider, child) =>
          FutureBuilder(
            future: curtainProvider.curtains,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                curtains = snapshot.data;
                return Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20, vertical: 0),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height * 100,
                  child: GridView.builder(
                    itemCount: curtains.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: isLarge(context) ? 0.9 : 0.9,
                        crossAxisCount: isLarge(context) ? 4 : 2,
                        crossAxisSpacing: 25,
                        mainAxisSpacing: 10),
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Consumer<CurtainProvider>(
                          builder: (context, value, child) =>
                              CurtainCard(
                                name: '${curtains[index].name}',
                                status: curtains[index].value,


                                colorStop: curtains[index].value != 'stop'
                                    ? Theme
                                    .of(context)
                                    .accentColor
                                    : Colors.red,
                                colorDown: curtains[index].value != 'down'
                                    ? Theme
                                    .of(context)
                                    .accentColor
                                    : Colors.red,

                                colorUp: curtains[index].value != 'up'
                                    ? Theme
                                    .of(context)
                                    .accentColor
                                    : Colors.red,


                                onDwon: () {
                                  value.setCurtainNumber(index);
                                  setState(() {
                                    curtains[index].value == 'down'
                                        ? curtains[index].value = 'down'
                                        : curtains[index].value = 'down';
                                  });
                                  updateCurtainValue(curtains[index].id,
                                      curtains[index]);
                                },
                                onUp: () {
                                  value.setCurtainNumber(index);

                                  setState(() {
                                    curtains[index].value == 'up'
                                        ? curtains[index].value = 'up'
                                        : curtains[index].value = 'up';
                                  });
                                  updateCurtainValue(curtains[index].id,
                                      curtains[index]);
                                },
                                onStop: () {
                                  value.setCurtainNumber(index);

                                  setState(() {
                                    curtains[index].value == 'stop'
                                        ? curtains[index].value = 'stop'
                                        : curtains[index].value = 'stop';
                                  });
                                  updateCurtainValue(curtains[index].id,
                                      curtains[index]);
                                },
                                onLongPress: () {
                                  curtainProvider.setCurtain(curtains[index]);
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return Container(
                                        // height:MediaQuery.of(context).size.height * 0.8,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment
                                              .start,
                                          //  mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _group,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Group ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _group = val,
                                              ),
                                            ), Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _zone,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Zone ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _zone = val,
                                              ),
                                            ), Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _button,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Button ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _zone = val,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                              children: [
                                                RaisedButton(
                                                  color: Colors.red,
                                                  child: const Text('Close'),
                                                  onPressed: () =>
                                                      Navigator.pop(context),
                                                ),
                                                RaisedButton(
                                                  color: Theme
                                                      .of(context)
                                                      .accentColor,
                                                  child: const Text('Done'),
                                                  onPressed: () {
                                                    value.setCurtainNumber(
                                                        index);
                                                    updateCurtainDetails(
                                                        Curtain(
                                                            group: int.tryParse(
                                                                _group.text),
                                                            zone: int.tryParse(
                                                                _zone.text),
                                                            button: int
                                                                .tryParse(
                                                                _button.text)));
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                },
                              ),
                        ),
                      );
                    },
                  ),
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            },
          ),
    );
  }

  Widget buildACs() {
    return Consumer<ACProvider>(
      builder: (context, acProvider, child) =>
          FutureBuilder(
            future: acProvider.acs,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                acs = snapshot.data;


                return Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20, vertical: 0),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height * 100,
                  child: GridView.builder(
                    itemCount: acs.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: isLarge(context) ? 1.2 : 1.1,
                        crossAxisCount: isLarge(context) ? 2 : 1,
                        crossAxisSpacing: 25,
                        mainAxisSpacing: 10),
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Consumer<ACProvider>(
                          builder: (context, value, child) =>
                              ACCard(
                                onremove: () {
                                  setState(() {
                                    if (acs[index].targetTemp != null)
                                      acs[index].targetTemp--;
                                    else
                                      acs[index].targetTemp = 0;
                                    updateACValue(acs[index].id,
                                        AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                        ,group: acs[index].group,id: acs[index].id));
                                  });
                                },


                                fanup: () {
                                  setState(() {
                                    if (acs[index].fanSpeed != null)
                                      acs[index].fanSpeed++;
                                    else
                                      acs[index].fanSpeed = 0;
                                    updateACValue(acs[index].id,
                                        AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                            ,group: acs[index].group,id: acs[index].id));
                                  });
                                }
                                ,
                                currentT: acs[index].currentTemp,
                                fandown: () {
                                  setState(() {
                                    if (acs[index].fanSpeed != null)
                                      acs[index].fanSpeed--;
                                    else
                                      acs[index].fanSpeed = 0;
                                    updateACValue(acs[index].id,
                                        AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                            ,group: acs[index].group,id: acs[index].id));
                                  });
                                }
                                ,
                                ModeIcon: acs[index].mode == 'heat'
                                    ? Icon(Icons.wb_sunny_outlined,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,)
                                    : acs[index].mode == 'cool'
                                    ? Icon(
                                  Icons.ac_unit, color: acs[index].value == 'on'
                                    ? Theme
                                    .of(context)
                                    .accentColor
                                    : Colors.red,)
                                    : acs[index].mode == 'fan'
                                    ? Icon(Icons.flip_camera_android_outlined,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,)
                                    : Icon(Icons.error_outline_rounded,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,),
                                onadd: () {
                                  setState(() {
                                    if (acs[index].targetTemp != null)
                                      acs[index].targetTemp++;
                                    else
                                      acs[index].targetTemp = 0;
                                    updateACValue(acs[index].id,
                                        AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                            ,group: acs[index].group,id: acs[index].id));
                                  });
                                },

                                name: '${acs[index].name}',
                                status: acs[index].value,
                                color: acs[index].value == 'on'
                                    ? Theme
                                    .of(context)
                                    .accentColor
                                    : Colors.red,


                                TargetT: acs[index].targetTemp,


                                onPower: () {
                                  value.setACNumber(index);
                                  acs[index].value == 'on'
                                      ? acs[index].value = 'off'
                                      : acs[index].value = 'on';
                                  updateACValue(acs[index].id,
                                      AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                          ,group: acs[index].group,id: acs[index].id));
                                },
                                onFan: () {
                                  value.setACNumber(index);
                                  if (acs[index].fanSpeed == 1) {
                                    acs[index].fanSpeed = 2;
                                  } else if (acs[index].fanSpeed == 2) {
                                    acs[index].fanSpeed = 3;
                                  } else if (acs[index].fanSpeed == 3) {
                                    acs[index].fanSpeed = 5;
                                  } else {
                                    acs[index].fanSpeed = 1;
                                  }

                                  updateACValue(acs[index].id,
                                      AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                          ,group: acs[index].group,id: acs[index].id));
                                },

                                fanIcon: acs[index].fanSpeed == 1
                                    ? Icon(Icons.looks_one,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,)
                                    : acs[index].fanSpeed == 2
                                    ? Icon(Icons.looks_two,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,) :
                                acs[index].fanSpeed == 3
                                    ? Icon(Icons.threed_rotation_rounded,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,) :
                                acs[index].fanSpeed == 5
                                    ? Icon(Icons.autorenew_outlined,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,) :
                                Icon(Icons.error_outline_rounded,
                                  color: acs[index].value == 'on'
                                      ? Theme
                                      .of(context)
                                      .accentColor
                                      : Colors.red,),

                                onMode: () {
                                  value.setACNumber(index);
                                  if (acs[index].mode == "heat") {
                                    acs[index].mode = "cool";
                                  } else if (acs[index].mode == "cool") {
                                    acs[index].mode = "fan";
                                  } else if (acs[index].mode == "fan") {
                                    acs[index].mode = "heat";
                                  }

                                  updateACValue(
                                      acs[index].id,AC(targetTemp: acs[index].targetTemp , value: acs[index].value,mode: acs[index].mode,fanSpeed: acs[index].fanSpeed,currentTemp: acs[index].currentTemp
                                      ,group: acs[index].group,id: acs[index].id));
                                },

                                onLongPress: () {
                                  acProvider.setAC(acs[index]);
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return Container(
                                        // height:MediaQuery.of(context).size.height * 0.8,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment
                                              .start,
                                          //  mainAxisSize: MainAxisSize.max,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _group,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Group ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _group = val,
                                              ),
                                            ), Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _zone,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Zone ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _zone = val,
                                              ),
                                            ), Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: TextField(
                                                  controller: _button,
                                                  style: TextStyle(
                                                    fontSize: 22,
                                                    color: AppColors
                                                        .accentColor_dark,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                  decoration: InputDecoration(
                                                    hintText: 'Button ID',
                                                    hintStyle: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .subtitle1,
                                                  )
                                                // onSaved: (val) => _zone = val,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                              children: [
                                                RaisedButton(
                                                  color: Colors.red,
                                                  child: const Text('Close'),
                                                  onPressed: () =>
                                                      Navigator.pop(context),
                                                ),
                                                RaisedButton(
                                                  color: Theme
                                                      .of(context)
                                                      .accentColor,
                                                  child: const Text('Done'),
                                                  onPressed: () {
                                                    value.setACNumber(index);
                                                    updateACDetails(
                                                        AC(group: int.tryParse(
                                                            _group.text),
                                                            zone: int.tryParse(
                                                                _zone.text),
                                                            button: int
                                                                .tryParse(
                                                                _button.text)));
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  );
                                },
                              ),
                        ),
                      );
                    },
                  ),
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            },
          ),
    );
  }


  Widget _buildList(int count) {
    return RefreshIndicator(
      onRefresh: _refreshData,
      child: builder(count),
    );
  }

  Timer timer;

  @override
  void initState() {
    super.initState();
/*
      timer = Timer.periodic(Duration(seconds: 1), (Timer t) => setState(() {





      var collection = Provider.of<CollectionProvider>(context).collection;

      Provider.of<DeviceProvider>(context).setDevices(
          _deviceServices.getDevicesByIds(collection.devices,context) );
      Provider.of<ACProvider>(context).setACs(
          _acServices.getACsByIds(collection.acs ,context) );
      Provider.of<CurtainProvider>(context).setCurtains(
          _curtainServices.getCurtainsByIds(collection.curtains,context ) );

    }));
*/

  }


  Future _refreshData() async {
    await Future.delayed(Duration(seconds: 1));
    setState(() async {
      var collection = Provider
          .of<CollectionProvider>(context)
          .collection;

      Provider.of<DeviceProvider>(context).setDevices(
          _deviceServices.getDevicesByIds(collection.devices, context));
      Provider.of<ACProvider>(context).setACs(
          _acServices.getACsByIds(collection.acs, context));
      Provider.of<CurtainProvider>(context).setCurtains(
          _curtainServices.getCurtainsByIds(collection.curtains, context));
    });
  }

  Widget builder(int current) {
    if (current == 0) {
      ctab = 0;
      return buildDevices();
    }
    else if (current == 1) {
      ctab = 1;
      return buildCurtains();
    }
    else if (current == 2) {
      ctab = 2;
      return buildACs();
    }
  }


  Widget ON_OFF(int current) {
    if (current == 0) {
      return Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        child: Wrap(
          alignment: WrapAlignment.spaceBetween,
          direction: Axis.horizontal,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Wrap(
              direction: Axis.horizontal,
              crossAxisAlignment: WrapCrossAlignment.center,
              // alignment: WrapAlignment.spaceAround,
              children: [
                Consumer<ThemeChanger>(
                  builder: (context, value, child) =>
                      CircularIconButton(
                        height: 32,
                        width: 32,
                        color: value.darkTheme
                            ? AppColors.iconsColorBackground3_dark
                            : AppColors.iconsColorBackground2_light,
                        child: Icon(Icons.lightbulb_outline),
                        onPressed: null,
                      ),
                ),
                Text('ON ALL / OFF ALL'),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: MaterialButton(
                onPressed: () {
                  for (Device device in devices) {
                    // _mqttServices.toggleDevice(
                    //     device.id, value ? 'on' : 'off');
                    device.value = value ? 'on' : 'off';
                    updateDeviceValue(device.id, device);
                    setState(() {
                      device.value = value ? 'on' : 'off';
                    });
                  }
                  setState(() {
                    value = !value;
                  });
                },
                padding: EdgeInsets.all(0),
                minWidth: 40,
                height: 40,
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Consumer<CollectionProvider>(
                  builder: (context, coll, child) =>
                      Hero(
                        tag: '${coll.collection.id}',
                        child: Container(
                          width: 40.0,
                          height: 40.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            // color: AppColors.primaryColor_dark,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.white.withAlpha(80),
                                offset: Offset(0, 3),
                                blurRadius: 6,
                              ),
                            ],
                          ),
                          child: Center(
                              child: Icon(
                                Icons.power_settings_new,
                                color: value
                                    ? Colors.red
                                    : Theme
                                    .of(context)
                                    .accentColor,
                                size: 30,
                              )),
                        ),
                      ),
                ),
              ),
            )
          ],
        ),
      );
    }
    else if (current == 1) {
      return Container();
    }
    else if (current == 2) {
      return Container();
    }
  }


  void updateACDetails(AC _ac) async {
    var acProvider = Provider.of<ACProvider>(context);
    if (await _acServices.updateAC(acProvider.ac.id, _ac, context)) {
      setState(() {
        // acs[acProvider.acNumber].name = _ac.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }

  void updateACValue(String id, AC _ac) async {

    Api.mqttServices.toggleAC(id, _ac);

    /*

    var acProvider = Provider.of<ACProvider>(context);
    if (await _acServices.updateAC(id, _ac,context)) {
      setState(() {

      });
      print("pin number:Updated");
    } else
      print('error');

     */
  }

  void updateDeviceDetails(Device _device) async {
    var deviceProvider = Provider.of<DeviceProvider>(context);
    if (await _deviceServices.updateDevice(
        deviceProvider.device.id, _device, context)) {
      setState(() {
        // devices[deviceProvider.deviceNumber].name = _device.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }

  void updateDeviceValue(String id, Device _device) async {
    //_device.name=null;
    Api.mqttServices.toggleDevice(id, Device(value: _device.value , button: _device.button,group: _device.group, zone: _device.zone));

    /*
    var deviceProvider = Provider.of<DeviceProvider>(context);
    if (await _deviceServices.updateDevice(id, _device,context)) {
      setState(() {
      });
      print("pin number:Updated");
    } else
      print('error');
  */

  }


  void updateCurtainDetails(Curtain _Curtain) async {
    var curtainProvider = Provider.of<CurtainProvider>(context);
    if (await _curtainServices.updateCurtain(
        curtainProvider.curtain.id, _Curtain, context)) {
      setState(() {
        //  devices[deviceProvider.deviceNumber].name = _device.name;
      });
      print("pin number:Updated");
    } else
      print('error');
  }

  void updateCurtainValue(String id, Curtain _Curtain) async {

    Api.mqttServices.toggleCurtain(id, Curtain(value: _Curtain.value , button: _Curtain.button,group: _Curtain.group, zone: _Curtain.zone));


    /*
    if (await _curtainServices.updateCurtain(id, _Curtain,context)) {
      setState(() {

      });
      print("pin number:Updated");
    } else
      print('error');

  */

  }


}
